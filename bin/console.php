<?php
define('APPDIR', realpath(__DIR__.'/..'));

// Include mandatory files
// and register autoloads
require APPDIR.'/vendor/autoload.php';
require APPDIR.'../autoload.php';
Autoloader::register();

/*
 * Sécurité, une erreur est si vite arrivée !
 */
if( __FILE__ !== realpath('.' . DIRECTORY_SEPARATOR . $_SERVER['argv'][0])) {
	die('Script name do not match. Abort.');
}

// Create Service Container
use Pimple\Container;
$container = new Container();
$container->register(new \SylvainLG\Training\PimpleProvider());

$container['log'] = function($c) {
	
	// $output = "[%datetime%] %channel%.%level_name%: %message%\n";
	// $formatter = new \Monolog\Formatter\LineFormatter($output);

	$streamHandler = new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::INFO);
	// $streamHandler->setFormatter($formatter);

	$log = new \Monolog\Logger('console');
	$log->pushHandler($streamHandler);
	
	return $log;
};

$router = $container['router'];
$router->any('strava:{method}', ['SylvainLG\Training\Console\StravaCommand','command']);

try {

	$resolver = new SylvainLG\Training\RouterResolver($container);
	$dispatcher = new Phroute\Phroute\Dispatcher($router->getData(), $resolver);
	$response = $dispatcher->dispatch('GET', $_SERVER['argv'][1]);
} catch(\Phroute\Phroute\Exception\HttpMethodNotAllowedException $e) {
	fwrite(STDERR, 'Method Not Allowed');
	fwrite($e->getMessage());
	exit;
}
