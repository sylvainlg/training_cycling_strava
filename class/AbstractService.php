<?php

namespace SylvainLG\Training;

/**
 * Classe AbstractService
 *
 * Met en place les fonctionnalités par défaut
 * d'un service: 
 *  * Container
 *  * Log
 */
abstract class AbstractService {

	/**
	 * Le ServiceContainer de l'application
	 */
	protected $_container;

	/**
	 * Le Logger de l'application
	 *
	 *  => Utile : debug_backtrace()[1]['function']; ?
	 */
	protected $_log;

	public function __construct($c) {
		$this->_container = $c;
		$this->_log = $this->_container['log'];

		$this->_log->info('Start service', [debug_backtrace()[1]['class']]);
	}

}