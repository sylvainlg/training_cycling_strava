<?php
namespace SylvainLG\Training;

/**
 * Charge la configuration globale de l'application
 * Cette configuration est contenu dans le répertoire config/
 * 
 * Le fichier config.json permet de faire la configuration globale et par défaut de l'application
 * Les environements ont des fichiers dédiés :
 *      dev.json
 *      prod.json
 */
class AppConfig {
	
    /**
     * Dossier contenant la configuration
     */
	private static $_CONFDIR = APPDIR . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

    /**
     * Environnement sélectionné
     * dev par défaut
     */
	private $_env = 'dev';

    /**
     * Variable contenant les configurations
     */
    private $_config = [];

    /**
    * Le constrcuteur charge la configuration depuis le fichier json
    **/
    public function __construct() {
        
		if(defined('APP_ENV')) {
			$this->_env = APP_ENV;
		}

		$base_config = json_decode(file_get_contents( self::$_CONFDIR . 'config.json'), true);
		$env_config = json_decode(file_get_contents( self::$_CONFDIR . $this->_env . '.json'), true);
		$this->_config = array_merge($base_config, $env_config);
    }

    /**
    *  Permet d'obtenir la valeur de la configuration
    *  @param $key string clef à récupérer
    *  @return mixed
    **/
    public function get($key) : string {
        // $this->_log->debug('config :: get $key:' . $key, ['__METHOD__'=>__METHOD__]);
        if (!isset($this->_config[$key])) {
            // $this->_log->debug('config :: get $key:' . $key . ' not found', ['__METHOD__'=>__METHOD__]);
            return null;
        }
        return $this->_config[$key];
    }

    /**
     * Permet d'obtenir l'ensemble de la configuration sous forme de tableau
     * @return array
     */
    public function getAll() : array {
        // $this->_log->debug('config :: getAll', ['__METHOD__'=>__METHOD__]);
        return $this->_config;
    }

}