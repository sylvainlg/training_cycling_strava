<?php

namespace SylvainLG\Training\Console;

class StravaCommand extends AbstractCommand {

	public function syncAction($argv) {
		$this->_container['log']->info('syncAction', ['__METHOD__'=>__METHOD__, 'argv'=>$argv]);

		$command = new \Commando\Command($argv);

		$command->option()
			->require()
			->describeAs('A Strava user token');
		// 06281543fbe556bceea7ae48412f9826a91a3e1e

		$command->option('dry-run')
			->describeAs('Dry for the action')
			->boolean();
		
		if($command['dry-run']) {
			$this->_container['log']->info('Enable dry run mode', ['__METHOD__'=>__METHOD__]);
		}

		$api = $this->_container['api'];
		$api->setAccessToken($command[0]);
		$athlete = $api->get('/athlete');

		$this->_container['athlete_id'] = intval($athlete->id);

		$this->_container['activity']->syncWithStrava();

		$this->_container['log']->info('syncAction end normally', ['__METHOD__'=>__METHOD__]);

	}

}