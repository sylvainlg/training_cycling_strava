<?php

namespace SylvainLG\Training;

trait ContainerAwareTrait {
	
	protected $_container;

	public function setContainer(\Pimple\Container $container) {
		$this->_container = $container;
	}
}