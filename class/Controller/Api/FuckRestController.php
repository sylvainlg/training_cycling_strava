<?php

namespace SylvainLG\Training\Controller\Api;

/**
 * Api Fuck
 * 
 * Expose une API Rest à la manière de Phroute
 *
 * @see https://github.com/mrjgreen/phroute#controllers
 * @see BaseRestController
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 */
class FuckRestController extends BaseRestController {

	public function _get($uuid) {
		return 'get ' . $uuid;
	}

	public function _post() {
		return 'post';
	}

	public function _put($uuid) {
		return 'put ' . $uuid;
	}

	public function _delete($uuid) {
		return 'delete ' . $uuid;
	}

	public function _prout() {
		return 'prout';
	}

}