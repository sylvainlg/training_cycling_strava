<?php

namespace SylvainLG\Training\Controller\Api;

use \SylvainLG\Training\Model\Planning;

/**
 * Api Planning
 * 
 * Expose une API Rest à la manière de Phroute
 *
 * @see BaseRestController
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 */
class PlanningRestController extends BaseRestController {

	/**
	 * getIndex
	 * 
	 * Expose le / et /index en get
	 * 
	 * @param optionnal planning UUID
	 * @return mixed le planning ou la liste des plannings
	 */
	protected function _get($planningUuid) {
		$this->_container['log']->debug('get', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);

		if($planningUuid === null) {
			$planningService = $this->_container['planning'];
			return $this->_format($planningService->all());
		} elseif(!is_string($planningUuid)) {
			$this->_container['log']->warning('Requested planning UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		} else {

			$planning = $this->_container['planning']->get($planningUuid);
			if(is_null($planning)) {
				$this->_container['log']->warning('Requested planning not found', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
				header('HTTP/1.1 404 Not found');
				exit;
			} else {
				$planning->date = $planning->date->format('U') * 1000;
				return $this->_format($planning);
			}

		}

	}

	/**
	 * postIndex
	 * HTTP POST
	 *
	 * Créé un nouveau planning
	 * 
	 * @return mixed well formatted planning
	 */
	protected function _post() {
		$this->_container['log']->debug('post', ['__METHOD__'=>__METHOD__]);

		/*
		 * Création de l'objet planning
		 * Vérfication des données
		 * Sauvegarde du planning
		 */
		$p = new Planning();

		$filtered_post = $this->parseRequest();

		$p->name 		= $filtered_post['name'];
		$p->start 		= $filtered_post['start'];
		$p->end 		= $filtered_post['end'];

		$planningService = $this->_container['planning'];
		$planningService->add($p);

		http_response_code(201);
		return $this->_format($p);

	}

	/**
	 * putIndex
	 * 
	 * HTTP PUT
	 * 
	 * @param $planningUuid
	 * @return mixed formatted and updated planning
	 */
	protected function _put($planningUuid) {

		/*
		 * Le uuid est-il présent
		 */
		if(is_null($planningUuid) or !is_string($planningUuid)) {
			$this->_container['log']->warning('Requested planning UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		}

		/*
		 * Récupération du planning
		 * Vérification que celui-ci existe
		 */
		$planningService = $this->_container['planning'];
		$planning = $planningService->get($planningUuid);
		if(is_null($planning)) {
			$this->_container['log']->warning('Requested planning not found', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
			header('HTTP/1.1 404 Not found');
			exit;
		}

		/*
		 * Do the work
		 */

		$filtered_post = $this->parseRequest();

		$planning->title 		= $filtered_post['title'];
		$planning->date 		= new \DateTime($filtered_post['date'].' 00:00:00');
		$planning->type 		= $filtered_post['type'];
		$planning->content 	= $filtered_post['content'];
		// $planning->notes 		= '';
		// $planning->duration 	= '';
		// $planning->difficulty 	= '';
		$planning->steps 		= $filtered_post['steps'];

		$planningService->set($planning);

		return $this->_format($planning);

	}


	 /**
	  * deleteIndex
	  * HTTP DELETE
	  *
	  * @param $planningUuid
	  */
	protected function _delete($planningUuid) {
		$this->_container['log']->debug('delete', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
		//planning_583cb75b0af219.55136114

		if(is_null($planningUuid) or !is_string($planningUuid)) {
			$this->_container['log']->warning('Requested planning UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		} else {

			$planning = $this->_container['planning']->get($planningUuid);
			if(is_null($planning)) {
				$this->_container['log']->warning('Requested planning not found', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
				header('HTTP/1.1 404 Not found');
				exit;
			} else {
				if($this->_container['planning']->delete($planning)) {
					$this->_container['log']->debug('Planning is deleted', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
					header('HTTP/1.1 204 No Content');
					exit;
				} else {
					$this->_container['log']->error('Planning is not deleted', ['__METHOD__'=>__METHOD__, 'id'=>$planningUuid]);
					header('HTTP/1.1 500 Internal Server Error');
					exit;
				}
			}

		}
	}

	/**
	 * Fait le boulot long et chiant de vérification des inputs
	 * Elle doit être utilisée par POST et PUT
	 * 
	 * @return array les inputs tous propres
	 */
	private function parseRequest() {

		$this->_container['log']->debug('parseRequest', ['__METHOD__'=>__METHOD__]);

		$post = [];

		/*
		 * Récupère le contenu de la requête post depuis le body
		 */
		$entityBody = file_get_contents('php://input');
		if(!empty($entityBody)) {

			/*
			 * Récupère et analyse le Content-Type
			 * Seul le application/json est actuellement accepté
			 * Sinon HTTP 415
			 */
			$contentType = $_SERVER['CONTENT_TYPE'];
			if($contentType !== 'application/json') {
				$this->_container['log']->warning('Post data are not application/json type ', ['__METHOD__'=>__METHOD__, 'Content-Type'=>$contentType]);
				header('HTTP/1.1 415 Unsupported Media Type');
				exit;
			}

			/*
			 * Lit des données json
			 * Si les données ne sont pas valides => HTTP 400
			 */
			$post = json_decode($entityBody, true);
			if($post === null or empty($post)) {
				$this->_container['log']->warning('Post data missing', ['__METHOD__'=>__METHOD__, 'Content-Type'=>$contentType]);
				header('HTTP/1.1 400 Bad request');
				exit;
			}

		}

		elseif (!empty($_POST)) {
			$this->_container['log']->warning('Post data are send throught $_POST variable not in body ', ['__METHOD__'=>__METHOD__]);
			$post = $_POST;
		}

		else {
			$this->_container['log']->warning('400 Bad Request, could not create planning', ['__METHOD__'=>__METHOD__]);
			header('HTTP/1.1 400 Bad request');
			exit;
		}

		/*
		 * Création de l'objet planning
		 * Vérfication des données
		 * Sauvegarde du planning
		 */
		$w = new Planning();

		$filters = [
			'name' => FILTER_SANITIZE_STRING,
			'start' => [	'filter' => FILTER_VALIDATE_REGEXP,
						'options' => [ 'regexp' => '#[0-9]{4}-[0-9]{2}-[0-9]{2}#' ]
					],
			'end' => [	'filter' => FILTER_VALIDATE_REGEXP,
						'options' => [ 'regexp' => '#[0-9]{4}-[0-9]{2}-[0-9]{2}#' ]
					],
		];

		$filtered_post = filter_var_array($post, $filters);

		return $filtered_post;
	}

}