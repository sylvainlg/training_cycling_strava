<?php

namespace SylvainLG\Training\Controller;

use SylvainLG\Training\Model\Workout;

class HomeController extends BaseController {

	public function index() {
		$this->_container['log']->debug('index', ['__METHOD__'=>__METHOD__]);

		$workouts = [];
		$workoutService = $this->_container['workout'];


		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type FROM workout WHERE DATE(date) = CURDATE()');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive today workouts', ['__METHOD__'=>__METHOD__]);
			exit; // TODO
			//throw new Exception();
		}
		$workouts['today'] = $workoutService->today();



		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type FROM workout WHERE DATE(date) = CURDATE() + INTERVAL 1 DAY');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive tomorrow workouts', ['__METHOD__'=>__METHOD__]);
			exit; // TODO
			//throw new Exception();
		}
		$workouts['tomorrow'] = $sth->fetchAll();
	

		/*
		 * Récupération des données pour le second bloc
		 *  - Workouts prévus cette semaine
		 * 	- Workouts réalisés cette semaine
		 */
		$workouts['thisweek'] = $workoutService->week();

		$workouts['thisweek_todo_count'] = 0;
		foreach($workouts['thisweek'] as $w) {
			if($w->activityId) {
				++$workouts['thisweek_todo_count'];
			} 
			
		}

		/*
		 * Récupération des données pour le troisième bloc
		 *  - A classer (commentaire, temps & co.)
		 */
		$workouts['to_classify'] = [];
		$workouts['classified'] = 0;
		$now = new \Datetime();
		$now->setTime(23,59,59);
		foreach($workouts['thisweek'] as $w) {
			if(new \Datetime($w->date) <= $now &&
				(!$w->notes &&
				!$w->duration &&
				!$w->difficulty &&
				(!$w->activityId))) {

				$workouts['to_classify'][] = $w;
			} elseif($w->date <= $now &&
				($w->notes or
				$w->duration or
				$w->difficulty)) {
				++$workouts['classified'];
			}
		}

		return $this->render('home.html.twig', [
			'workouts' => $workouts,
			'activitiesCount' => $this->_container['activity']->count()
		]);
	}


	public function events() {
		$this->_container['log']->debug('index', ['__METHOD__'=>__METHOD__]);

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			// TODO faire des vérifications sur les inputs

			var_dump($_POST);
			$event = new \SylvainLG\Training\Model\Event();
			$_POST['uuid'] = $event->uuid;
			$event->fromArray($_POST);

			$this->_container['event']->add($event);

			return $this->redirect('home_event');

		}

		return $this->render('home/events.html.twig', [
			'events' => $this->_container['event']->all(),
			'activitiesCount' => $this->_container['activity']->count()
		]);
	}

	public function me() {

		$this->_container['log']->debug('me', ['__METHOD__' => __METHOD__]);

		return $this->render('home/me.html.twig', [
			'zones' => $this->_container['userservice']->getZones(),
			'activitiesCount' => $this->_container['activity']->count()
		]);

	}

	public function forme() {

		$this->_container['log']->debug('forme', ['__METHOD__' => __METHOD__]);

		return $this->render('home/forme.html.twig', [
			// 'banister' => $this->_container['activity']->banister(),
			'tsb' => $this->_container['activity']->tsb(),
			'activitiesCount' => $this->_container['activity']->count()
		]);
	}

	public function prout() {

		$this->_container['log']->debug('prout', ['__METHOD__' => __METHOD__]);

		$json = json_decode(file_get_contents(APPDIR . '/mock/activity.json'));


$a = array_filter($json->streams, function($it) { return $it->type === 'time'; });
$b = array_pop($a);
$streams_time = $b->data;

$a = array_filter($json->streams, function($it) { return $it->type === 'heartrate'; });
$b = array_pop($a);
$streams_hr = $b->data;

$a = array_filter($json->streams, function($it) { return $it->type === 'velocity_smooth'; });
$b = array_pop($a);
$streams_speed = $b->data;

		$count = count($streams_time);
		$dat = [];
		for($i=0; $i<$count; ++$i) {
			$vel = $streams_speed[$i] * 3.6;
			$hour = gmdate('H:i:s', $streams_time[$i]);
			$dat[$streams_time[$i]] = [
				'data' => "['$hour', $streams_hr[$i], $vel]"
			];
		}

		return $this->render('home/prout.html.twig', [
			'tsb' => $dat
		]);

	}

}