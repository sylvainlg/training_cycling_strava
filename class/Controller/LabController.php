<?php
namespace SylvainLG\Training\Controller;

use \Cmfcmf\OpenWeatherMap;
use \Cmfcmf\OpenWeatherMap\Exception as OWMException;

class LabController extends BaseController {

	private $lab = [];

	/**
	 * Constructeur
	 * 
	 * Sert principalement à ajouter les fonctionnalités Lab
	 */
	public function __construct() {
		// parent::__construct();

		// Create OpenWeatherMap object. 
		// Don't use caching (take a look into Examples/Cache.php to see how it works).
		$this->lab['api.weather'] = new OpenWeatherMap('eaf6eeacba60ebee6400097de08a622a');
// var_dump($_SESSION['strava']->access_token);
		
	}


	public function index() {
		$this->_container['log']->debug('index', ['__METHOD__'=>__METHOD__]);

		// Language of data (try your own language here!):
		$lang = 'fr';

		// Units (can be 'metric' or 'imperial' [default]):
		$units = 'metric';

		try {
			$weather = $this->lab['api.weather']->getWeather('Guipavas', $units, $lang);
		} catch(OWMException $e) {
			echo 'OpenWeatherMap exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
			throw $e;
		} catch(\Exception $e) {
			echo 'General exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
			throw $e;
		}

		return $this->render('lab/index.html.twig', [
			'weather' => $weather,
		]);
	}

	public function mongo() {

		return $this->render('lab/mongo.html.twig', [
			'coll' => $this->_container['activity']->get(),
		]);

	}

}