<?php

namespace SylvainLG\Training\Controller;

/**
 * Ce controler gère le cycle de vie de l'authentification des utilisateurs.
 * 
 * Important : les sessions PHP doivent être gérées dans un redis pour pouvoir
 * monter en charge !
 */
class LoginController extends BaseController {

	/**
	 * Affiche la page permettant à l'utilisateur de se connecter avec Strava
	 */
	public function index() {
		$this->_container['log']->debug('index', ['__METHOD__'=>__METHOD__]);

		if(isset($_SESSION['strava']->access_token) 
		 && !empty($_SESSION['strava']->access_token)) {
			$this->redirect('home', []);
			exit;
		}

		$api = $this->_container['api'];

		$domain = $this->_container['config']->get('serverurl');

		$authorizationUrl = $api->authenticationUrl($domain . $this->_container['router']->route('token_exchange', []), 'auto', 'view_private,write');

		return $this->render('login/index.html.twig', [
			'authorizationUrl' => $authorizationUrl,
		]);
	}

	/**
	 * Cette méthode fait l'échange de jeton avec Strava.
	 * Elle récupère le véritable jeton OAuth depuis l'API Strava.
	 */
	public function tokenExchange() {
		
		$code = $_GET['code'];

		$api = $this->_container['api'];
		$_SESSION['strava'] = $api->tokenExchange($code);

		$this->_container['user']->storeAccessToken($_SESSION['strava']->athlete->id, $_SESSION['strava']->access_token);

		// Créer un event ?? 
		
		$this->redirect('home', []);
	}

	/**
	 * Supprime les données de session,
	 * cela déconnecte l'utilisateur.
	 */
	public function logout() {
		session_destroy();
		$this->redirect('home', []);
		exit;
	}

}