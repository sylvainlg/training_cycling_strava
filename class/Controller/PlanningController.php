<?php

namespace SylvainLG\Training\Controller;

use \SylvainLG\Training\Model\Planning;
use \SylvainLG\Training\Model\Workout;

class PlanningController extends BaseController {

	public function index() {
		
		// Récupération du planning
		$planningService = $this->_container['planning'];		
		$slots = $planningService->all();

		/*
		 * Récupération des workouts de l'année
		 */
		$sth = $this->_container['mysql']->prepare(
			'SELECT * FROM workout WHERE athlete=? AND (date BETWEEN "2017-11-01" AND "2018-10-31")'
		);
		$ret = $sth->execute([
			$this->_container['athlete_id']
		]);

		if(!$ret) {
			$this->_log->error('Cannot search workouts', ['__METHOD__'=>__METHOD__, $w,  $sth->errorInfo()]);
			throw new \Exception('Cannot search workouts');
		}

		$res = $sth->fetchAll(\PDO::FETCH_CLASS, Workout::class, ['hydrate'=>true]);

		$workouts = [];
		foreach($res as $w) {
			$workouts[(new \Datetime($w->date))->format('Y-m-d')][] = $w;
		}

		// Jour vide pour commencer
		$prevd = [];


		// Définition des dates limites
		$dt = new \Datetime('1 november 2017');
		$end = new \Datetime('31 october 2018');
		$P1D = new \DateInterval('P1D');

		// Semaine courante
		$cur_week = $dt->format('W');

		// Données journalières des positions 
		// et des contenus des slots
		$data = [];

		// Légende par mois
		$legends = [];

		// Variables pour le calcul des résumés de semaines
		$first = true;
		$week_summaries = [];
		$P1W = new \DateInterval('P1W');
		$tmp = [];

		// GO MAGIC	
		for(;$dt<=$end; $dt->add($P1D)) { // Pour chaque jour

			$month = $dt->format('Y-m');

			if(!isset($legends[$month])) {
				$legends[$month] = [];
			}

			// Si on change de semaine
			if( $dt->format('W') != $cur_week ) {

				// Alors on change la semaine courante
				$cur_week = $dt->format('W');

				// On reset le jour précédent car
				// plus pertinent en ce début de nouvelle semaine 
				$prevd = [];

			}

			// Current day
			$cd = [];

			// Copie du planning
			$tmpslots = $slots;

			// Parcours des slots déjà positionnés (au sens html)
			foreach($prevd as $k=>$prevslot) {
				// Rien le premier jour

				foreach( $tmpslots as $j=>$slot ) {

					if( $prevslot->equalsTo($slot) && (new \Datetime($slot->end)) >= $dt ) {
						// Match de slot
						//  On repositionne le slot de la même façon
						$cd[$k] = $slot;

						$this->add_dedup($legends[$month], $slot);

						// Suppression du slot des slots à traiter ultérieurement
						unset($tmpslots[$j]);
					}

				}

			}

// var_dump($dt);
// var_dump($cd);

			// Calcul du poids du slot sur la fin de semaine
			// 1 "points" = 1 jour
			$weights = [];
			foreach($tmpslots as $slot) {
				$weights[$slot->uuid] = 0;

				$e = new \Datetime($slot->end);

				// if($slot->start > $dt && $slot->end < $dt) {
				if( $dt >= new \Datetime($slot->start) && $dt <= $e ) {
					// Si le slot est commencé et n'est pas terminé

					// Calcul du temps d'aujourd'hui à la fin du slot
					$diff = $e->diff($dt, true)->days + 1; // Le jour courant est compté comme 1 jour

					// Calcul du poids 
					$weights[$slot->uuid] = max($diff, 7-$dt->format('N')-1);
				}
			}

			// Tri sur le poids des slots
			usort($tmpslots, function($a, $b) use ($weights) {
				$wa = $weights[$a->uuid];
				$wb = $weights[$b->uuid];

				if ($wa == $wb) {
					return 0;
				}
				return ($wa > $wb) ? -1 : 1;
			});

			// Traitement des slots restant pour la journée
			foreach($tmpslots as $k=>$slot) {
				// Pour chaque slot non positionné le jour
				// précédent, il faut lui trouver une place

				$s = new \Datetime($slot->start);
				$e = new \Datetime($slot->end);

				if($s > $dt || $e < $dt) {
					// Si le slot n'est pas commencé ou déjà terminé
					// skip it
					continue;
				}

				// On parcours les index du tableau du jour courant pour rechercher les "trous"
				for($i=0; $i<count($cd); ++$i) {

					// Si le $cd[$i] est vide,
					// on y ajoute le slot
					if(!isset($cd[$i])) {
						$cd[$i] = $slot;
						$this->add_dedup($legends[$month], $slot);

						break;// Arrête le for $cd[$i]
					}

				}

				// Si toutes les cases de $cd
				// sont pleines alors on empile 
				if($i >= count($cd) &&
					$dt >= $s && 
					$dt <= $e) {

					$cd[] = $slot;
					$this->add_dedup($legends[$month], $slot);
					
				}

			}
			
			$data[$dt->format('Y-m-d')] = $cd;
			$prevd = $cd;


			/////////////////////////////////////////
			/*
			 * Calcul des sommaires de semaines
			 */

			if(isset($workouts[$dt->format('Y-m-d')])) {
				$tmp = array_merge($tmp, $workouts[$dt->format('Y-m-d')]);
			}

			// En fin de semaine => bilan
			if($dt->format('N') == 7) {
				if($first === true) {
					$first = false;
					$week_summaries[(new \Datetime('1 november 2017'))->format('Y-m-d')] = $tmp;
				} else {
					$start_of_week = (new \Datetime())->setTimestamp($dt->getTimestamp())->sub(new \DateInterval('P6D'));
					$week_summaries[$start_of_week->format('Y-m-d')] = [
						'workouts' 	=> $tmp,
						'charge'	=> array_reduce($tmp, function($carry, $w) {
							return $carry + $this->_container['workout']->esie($w);
						}, 0) ?? 0,
						'realcharge' => array_reduce($tmp, function($carry, $w) {
							return $carry + $this->_container['workout']->realesie($w);
						}, 0) ?? 0,
					];
				}

				$tmp = [];
			}

		}

		// var_dump($week_summaries);exit;


		// Construction du tableau des mois de la saison
		$month = 11;
		$year = 2017;
		$months = [];
		for($i=0;$i<=11;++$i) {
			$months[] = $year.'-'.str_pad($month,2,'0',STR_PAD_LEFT);
			$month = ++$month % 13;
			if($month === 0) {
				$month = 1;
				++$year;
			}
		}


		/*
		 * TODO
		 * 
		 * Calculer le nombre d'entrainements pour chaque semaine => OK
		 * Calculer la charge théorique pour ces entrainements => En ISIE
		 * Si des activités sont liées, calculer la charge réelle
		 * Calculer l'indicateur de "réussite" moyen des entrainements
		 * What else ?
		 */
		
		
		// Rendu
		return $this->render(
			'calendar/index.html.2.twig',
			[
				// 'assoc_date' => $assoc_date,
				'months' => $months,
				'data' => $data,
				'legend' => $legends,
				'workouts' => $workouts,
				'week_summaries' => $week_summaries
			]
		);

	}

	private function add_dedup(&$arr, Planning $slot) {

		foreach($arr as $s) {
			if($s->equalsTo($slot)) {
				return;
			}
		}

		$arr[] = $slot;

	}
}