<?php

namespace SylvainLG\Training\Controller;

class SettingsController extends BaseController {

	public function index() {
		$settingsService = $this->_container['settings'];
		
		if(isset($_POST['edit'])) {

			$k = $_POST['edit'];
			$v = $_POST['formConfValue'];
			$settingsService->set($k, $v);
			
			header('Location: /settings');
			exit;

		} elseif(isset($_POST['create'])) {
			
			$k = $_POST['formCreateConfName'];
			$v = $_POST['formCreateConfValue'];
			$settingsService->set($k, $v);
			
			header('Location: /settings');
			exit;

		}

		$conf = $settingsService->getAll();
		return $this->render('settings/index.html.twig', ['conf'=>$conf]);

	}
}