<?php

namespace SylvainLG\Training\Controller;

class WorkoutController extends BaseController {

	public function index() {

		$start = new \Datetime();
		$start->setTime(0,0,0);

		$end = new \Datetime();
		$end->add(new \DateInterval('P1D'));
		$end->setTime(0,0,0);


		$workouts = [];

		$workoutService = $this->_container['workout'];
		$workouts['today'] = $workoutService->today();
		$workouts['tomorrow'] = $workoutService->tomorrow();
		$workouts['todo'] = $workoutService->todo();
		$workouts['notstrava'] = $workoutService->noStrava();

		return $this->render('workout/index.html.twig',
		[
			'workouts' => $workouts,
			'route_name' => 'workout',
		]);
	}

	public function list() {
		$workoutService = $this->_container['workout'];

		$filter = $_GET['filter'] ?? 'all';
		
		switch($filter) {

			case 'week':
				$workouts = $workoutService->week();
				break;
			
			case 'all':
			default:
				$workouts = $workoutService->all();

		}
			

		return $this->render('workout/list.html.twig',
			[
				'workouts' => $workouts,
				'route_name' => 'workout_list',
			]
		);
	}

	public function creator() {
		return $this->render('workout/creator.html.twig',
			[
				'route_name' => 'workout_creator',
			]
		);
	}

	public function creatorRide() {
		return $this->render('workout/creator.ride.html.twig',
			[
				'route_name' => 'workout_creator',
			]
		);
	}

	public function creatorWorkout() {
		return $this->render('workout/creator.workout.html.twig',
			[
				'route_name' => 'workout_creator',
			]
		);
	}

	public function add() {
		// TODO harmoniser avec l'API
		$w = new \SylvainLG\Training\Model\Workout();

		$w->title		= $_POST['title'];
		$w->date 		= $_POST['workoutdate'];
		$w->type 		= $_POST['type'];
		$w->content 	= $_POST['content'];
		$w->notes 		= '';
		$w->duration 	= '';
		$w->difficulty 	= '';

		$workoutService = $this->_container['workout'];
		$workoutService->add($w);

		header('Location: /workout');
		exit;

	}

	public function edit($id) {

		$workoutService = $this->_container['workout'];
		$w = $workoutService->get($id);

		if(is_null($w)) {
			throw new \Exception('Workout not found');
		}

		if(isset($_POST) && !empty($_POST)) {

			$post = (object) $_POST;

			$w->title 		= $post->title ?? $w->title;
			$w->date 		= (property_exists($post, 'date') && isset($post->date)) ? \DateTime::createFromFormat('Y-m-d', $post->date) : $w->date;
			$w->type 		= $post->type ?? $w->type;
			$w->content 	= $post->content ?? $w->content;
			$w->notes 		= $post->notes;
			$w->duration 	= $post->duration;
			$w->difficulty 	= $post->difficulty;

			$workoutService->set($w);

			return $this->redirect('workout_detail', [$w->uuid]);

		}

		return $this->render('workout/edit.html.twig', [
			'w'=>$w,
			'route_name' => 'workout',
		]);

	}

	public function delete($id) {
		
		$workoutService = $this->_container['workout'];
		$w = $workoutService->get($id);

		if($w === false || is_null($w)) {
			$this->_log->error('Workout non trouvé', ['__METHOD__'=>__METHOD__, 'w'=> $id]);
			$this->hey404();
		}

		$workoutService->delete($w);

		return $this->redirect('workout', []);
	}

	public function detail($uuid) {
		// TODO rendre cette méthode un peu propre ...
		$this->_container['log']->debug('detail', ['__METHOD__'=>__METHOD__]);
		
		if(empty($uuid) or !is_string($uuid)) {
			$this->_container['log']->warning('Requested workout UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$uuid]);
			return $this->_container['errorpage']->hey400();
		}

		$workout = $this->_container['workout']->get($uuid);

		if(is_null($workout) or $workout === false) {
			$this->_container['log']->warning('Requested workout not found', ['__METHOD__'=>__METHOD__, 'id'=>$uuid]);
			return $this->_container['errorpage']->hey404();
		}

		/*
		 * Données de match des activités
		 */
		$matches = [];

		/*
		 * Est ce que le matching a dû être élargi pour essayer de trouver des
		 * activités compatibles ?
		 * 
		 * Un paramétre ?extended=true permet d'activer cette fonctionnalité
		 */
		$extendedMatch = isset($_GET['extended']) && $_GET['extended'] == 'true';

		/*
		 * On essaye de trouver des match Strava aka activités si il n'est pas déjà réalisé
		 */
		$types = [
			'ppg' 		=> 'Workout',
			'ride'		=> 'Ride',
		];

		$db = $this->_container['db'];

		if($workout->activityId === null) {

			if(!$extendedMatch) {
				$sth = $this->_container['mysql']->prepare(
					'SELECT * FROM training_activity WHERE athlete=? AND DATE(start_date)=DATE(?) AND type=?');
				$sth->execute([
					$this->_container['athlete_id'],
					$workout->date,
					$types[strtolower($workout->type)]
				]);
				$matches = $sth->fetchAll();
				
			}

			if(count($matches) === 0) {

				$extendedMatch = true;

				$sth = $this->_container['mysql']->prepare(
					'SELECT * FROM training_activity WHERE athlete=? AND (DATE(start_date) BETWEEN DATE(?) - INTERVAL 1 DAY AND DATE(?) + INTERVAL 1 DAY)');
				$sth->execute([
					$this->_container['athlete_id'],
					$workout->date,
					$workout->date
				]);
				$matches = $sth->fetchAll();

			}

		}
		
		return $this->render('workout/detail.html.twig', [
			'workout' 	=> $workout, 
			'matches' 	=> $matches,
			'extended' 	=> $extendedMatch,
			'route_name' => 'workout',
		]);

	}

	/**
	 * Ecran de validation du match Activity <=> Workout
	 * 
	 * @param $uuid Workout uuid
	 * @param $activityId Activity
	 */
	public function match($uuid, $activityId) {

		// TODO check

		$workout = $this->_container['workout']->get($uuid);
		$activity = $this->_container['activity']->get($activityId);

		return $this->render('workout/match.html.twig', [
			'workout'=>$workout, 
			'activity'=>$activity,
			'route_name' => 'workout',
		]);

	}

	/**
	 * Validation effective du match Activity <=> Workout
	 * 
	 * @param $uuid Workout uuid
	 * @param $activityId Activity
	 */
	public function matchValidate($uuid, $activityId) {

		// TODO check

		$workoutService = $this->_container['workout'];
		$workout = $workoutService->get($uuid);
		// $activity = $this->_container['activity']->get($activityId);
		$workout->activityId = $activityId;
		$workoutService->set($workout);

		return $this->redirect('workout_detail', [
			'uuid'=>$workout->uuid,
		]);

	}

	/**
	 * Délie une association Activity <=> Workout
	 *
	 * @param $uuid Workout uuid
	 */
	public function unmatch($uuid) {

		$workoutService = $this->_container['workout'];
		$workout = $workoutService->get($uuid);
		$workout->activityId = null;
		$workoutService->set($workout);

		return $this->redirect('workout_detail', ['uuid'=>$workout->uuid]);
	}

	/**
	 * Ecran permettant de créer une activité à partir de son workout directement/manuellement.
	 * 
	 * @param $uuid Workout
	 * @return ?
	 */
	public function workoutToActivity($uuid) {
		// TODO check

		$workoutService = $this->_container['workout'];
		$workout = $workoutService->get($uuid);
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			

			// TODO check inputs 

			$time = $_POST['duration'];

			$dt = new \Datetime($_POST['start_date_local']);

			/*
			 * Création de l'activité
			 */
			$api = $this->_container['api'];
			$activity = $api->post('activities', [
				'name'             => $_POST['name'],
				'type'             => $_POST['type'],
				'start_date_local' => $dt->format('Y-m-d\TH:i:s\Z'),
				'elapsed_time'     => strtotime("1970-01-01 $time UTC"),
				'description'	   => $_POST['description'],
			]);

			if(isset($activity->errors)) {
				#object(stdClass)#81 (2) { ["message"]=> string(19) "Authorization Error" ["errors"]=> array(1) { [0]=> object(stdClass)#82 (3) { ["resource"]=> string(11) "AccessToken" ["field"]=> string(16) "write_permission" ["code"]=> string(7) "missing" } } }
				$this->_container['log']->error($activity->message, ['__METHOD__'=>__METHOD__, 'detail'=>$activity->errors]);
				throw new \Exception($activity->message);

			}

			/*
			 * Enregistrement de l'associtation du workout
			 * et de l'activité
			 */
			$workout->activityId = $activity->id;
			$workoutService->set($workout);

			/*
			 * Synchronisaiton des détails de l'activité
			 */
			$this->_container['activity']->syncOneWithStrava($activity->id);
			return $this->redirect('workout_detail', ['uuid'=>$workout->uuid]);

		}

		else {

			return $this->render('workout/workout_to_activity.html.twig',[
				'workout' => $workout,
				'route_name' => 'workout',
			]);

		}
		

	}

}