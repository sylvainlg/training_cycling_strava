<?php

namespace SylvainLG\Training\Model;

/**
 * Classe de données d'un événements
 * 
 * Représente les événements extérieurs au monde du vélo (rdv médicaux, contraintes récupération - travail, réunion, déplacement-, ...)
 */

class Event implements \JsonSerializable, \MongoDB\BSON\Persistable {

	private $uuid;
	private $title;
	private $date;
	private $content;
	
	public function __construct() {
		$this->uuid = uniqid('event_', true);
	}

	public function __get($key) {
		return $this->{$key};
	}

	public function __set($key, $value) {

		if($key=='uuid') {
			return;
		}

		$this->{$key} = $value;
		return $this;
	}

	public function __isset($key) {
		return array_key_exists($key, get_object_vars($this));
	}

	public function fromArray($arr) {

		if(!isset($arr['uuid']) or empty($arr['uuid'])) {
			throw new \Exception('UUID missing');
		}

		$this->uuid = $arr['uuid'];
		$this->title = $arr['title'] ?? 'Event - No name';
		$this->date = new \DateTime($arr['date']);
		$this->content = $arr['content'];
		
		return $this;

	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}

	
	public function bsonSerialize() {
		$keys = get_object_vars($this);

		// Date
		$keys['date'] = new \MongoDB\BSON\UTCDateTime($this->date->setTime(0,0,0)->getTimestamp()*1000);

		return $keys;
	}

	public function bsonUnserialize ( array $map ) {
		$dtz = new \DateTimeZone(date_default_timezone_get());
		foreach ( $map as $k => $value ) {
			if($k == 'date') {
				$this->$k = $value->toDateTime()->setTimeZone($dtz);
			} else {
				$this->$k = $value;
			}
		}
	}

}
