<?php

namespace SylvainLG\Training\Model;

class User {

	private $id;
	private $access_token;

	public function __get($key) {
		return $this->{$key};
	}

	public function __set($key, $value) {
		$this->{$key} = $value;
		return $this;
	}

	public function __isset($key) {
		return array_key_exists($key, get_object_vars($this));
	}

}