<?php

namespace SylvainLG\Training\Model;

/**
 * Classe de données d'un entraînement
 */

class Workout implements \JsonSerializable {

	private $uuid;
	private $title;
	private $date;
	private $type;
	private $content;
	private $notes;
	private $duration;
	private $difficulty;

	private $steps;

	/**
	 * Référence vers le id de l'activité (Strava) liée
	 */
	private $activityId;

	public function __construct() {
		$this->uuid = uniqid('workout_', true);
	}

	public function __get($key) {
		return $this->{$key};
	}

	public function __set($key, $value) {

		if($key=='uuid') {
			return;
		}

		$this->{$key} = $value;
		return $this;
	}

	public function __isset($key) {
		return array_key_exists($key, get_object_vars($this));
	}

	public function fromArray($arr) {

		if(!isset($arr['uuid']) or empty($arr['uuid'])) {
			throw new \Exception('UUID missing');
		}

		$this->uuid = $arr['uuid'];
		$this->title = $arr['title'] ?? 'No name';
		$this->date = $arr['date'];
		$this->type = $arr['type'];
		$this->content = $arr['content'];
		$this->notes = $arr['notes'];
		$this->duration = $arr['duration'];
		$this->difficulty = $arr['difficulty'];
		$this->activityId = $arr['activityId'] ?? null;
		$this->steps = $arr['steps'] ?? [];

		return $this;

	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}

	/*
	 * Pas accès à $container
	 */
	/*
	public function save() {

		if(!isset($this->uuid) or empty($this->uuid)) {
			$this->generateUuid();
		}

		$container['workout']->set($this);
	}
	*/

}
