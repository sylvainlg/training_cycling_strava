<?php

namespace SylvainLG\Training\Model;

use Ramsey\Uuid\Uuid;

/**
 * Classe de données d'un entraînement
 */

class Workout implements \JsonSerializable {

	private $uuid;
	private $title;
	private $date;
	private $type;
	private $content;
	private $notes;
	private $duration;
	private $difficulty;
	private $athlete;

	private $steps;

	/**
	 * Référence vers le id de l'activité (Strava) liée
	 */
	private $activityId;

	public function __construct($hydrate = false) {
		if($hydrate !== true) {
			$this->uuid = 'workout_' . Uuid::uuid4()->toString();
		}
	}

	public function __get($key) {
		return $this->{$key};
	}

	public function __set($key, $value) {

		if($key=='uuid') {
			//return;
		}

		if($key === 'steps') {
			$this->{$key} = is_array($value) or is_object($value) ? json_encode($value) : json_decode($value);
			return $this;
		}

		$this->{$key} = $value;
		return $this;
	}

	public function __isset($key) {
		return array_key_exists($key, get_object_vars($this));
	}

	public function fromArray($arr) {

		if(!isset($arr['uuid']) or empty($arr['uuid'])) {
			throw new \Exception('UUID missing');
		}

		$this->uuid = $arr['uuid'];
		$this->title = $arr['title'] ?? 'No name';
		$this->date = new \DateTime($arr['date']);
		$this->type = $arr['type'];
		$this->content = $arr['content'];
		$this->notes = $arr['notes'];
		$this->duration = $arr['duration'];
		$this->difficulty = $arr['difficulty'];
		$this->activityId = $arr['activityId'] ?? null;
		$this->steps = $arr['steps'] ?? [];
		$this->athlete = is_array($arr['athlete']) ? $arr['athlete'] : [$arr['athlete']];

		return $this;

	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}

}
