<?php

namespace SylvainLG\Training;

use \Pimple\Container;
use \MongoDB\Client as MongoClient;

class PimpleProvider implements \Pimple\ServiceProviderInterface {

	public function register(Container $container) {

		$container['config'] = function($c) {
			return new \SylvainLG\Training\AppConfig($c);
		};

		$container['db'] = function($c) {
			return (new MongoClient(getenv('MONGODB_ADDON_URI'),[],['typeMap' => ['array' => 'array']]))->{getenv('MONGODB_ADDON_DB')};
		};

		$container['workout'] = function($c) {
			return new \SylvainLG\Training\Service\WorkoutService($c);
		};

		$container['activity'] = function($c) {
			return new \SylvainLG\Training\Service\ActivityService($c);
		};

		$container['event'] = function($c) {
			return new \SylvainLG\Training\Service\EventService($c);
		};

		$container['planning'] = function($c) {
			return new \SylvainLG\Training\Service\PlanningService($c);
		};

		$container['userservice'] = function($c) {
			return new \SylvainLG\Training\Service\UserService($c);
		};

		$container['api'] = function($c) {
			$strava = new \Iamstuartwilson\StravaApi(
				'3415',
				'60ccbadb4c0e6348ae6f879f3cb70524f96226d1'
			);

			if(isset($_SESSION['strava'])) {
				$strava->setAccessToken($_SESSION['strava']->access_token);
			}

			return $strava;
		};

		$container['twig'] = function($c) {

			$loader = new Twig_Loader_Filesystem(APPDIR . '/templates');
			
			$twigHelper = new \SylvainLG\Training\TwigHelper();
			$twigHelper->setContainer($c);
			
			$twigExtension = new \SylvainLG\Training\TwigExtension();
			$twigExtension->setContainer($c);

			$twig = new Twig_Environment($loader, array(
				// 'cache' => APPDIR . '/twig_cache',
				'cache' => false,
				'debug' => true,
			));
			$twig->addGlobal('helper', $twigHelper);
			$twig->addExtension($twigExtension);

			return $twig;
		};

		$container['router'] = function($c) {
			return new \Phroute\Phroute\RouteCollector();
		};

		$container['accept'] = function($c) {
			return new \Trii\HTTPHeaders\Accept($_SERVER['HTTP_ACCEPT']);
		};

	}

}