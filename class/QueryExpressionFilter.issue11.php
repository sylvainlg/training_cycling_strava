<?php

namespace SylvainLG\Training;

/**
 * Surcharge de la classe QueryExpressionFilter
 * 
 * Ajout de la prise en compte des comparaisons sur \Datetime
 * 
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 * @see https://github.com/clue/php-json-query
 */
class QueryExpressionFilter extends \Clue\JsonQuery\QueryExpressionFilter {

    /**
     * Constructeur ajoutant des comparateurs
     * au module de base.
     * 
     * @see parent::__construct
     */
    public function __construct($queryExpression) {
        parent::__construct($queryExpression);
        $this->comparators['$dateeq'] = function($actualValue, $expectedValue) {

            /*
             * Les variables doivent être des dates \Datetime
             */
            if($actualValue instanceof \Datetime &&
                $expectedValue instanceof \Datetime) {

                    // Zero des heures
                    $actualValue->setTime(0,0,0);
                    $expectedValue->setTime(0,0,0);
                    
                    return $actualValue === $expectedValue;
                    
                }
            
            return false;
        };
    }

    /**
     * Ajout de la détection de la comparaison de date
     * 
     * @see parent::matchComparator 
     */
    public function matchComparator($actualValue, $comparator, $expectedValue) {

        if($expectedValue instanceof \Datetime) {
            $actualValue = new \Datetime($actualValue);
        }

        return parent::matchComparator($actualValue, $comparator, $expectedValue);

    }

}