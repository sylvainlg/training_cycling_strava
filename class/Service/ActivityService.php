<?php
namespace SylvainLG\Training\Service;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('HOST', 'golden-kangaroo.rmq.cloudamqp.com');
define('PORT', 5672);
define('USER', 'fhafaefr');
define('PASS', 'oJrLBc1E8kSbh4ulOTiZeO5HsLwHacNU');
define('VHOST', 'fhafaefr');
//If this is enabled you can see AMQP output on the CLI
//define('AMQP_DEBUG', true);

// TOP 10 TRIMP : select * from training_activity order by trimp desc limit 10 ;

/**
 * Classe ActivityService
 *
 * Service permettant de gérer les activités
 *
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 */

class ActivityService extends \SylvainLG\Training\AbstractService {

	/**
	 * Constructeur
	 */
	public function __construct($c) {
		parent::__construct($c);

	}

	/**
	 * Récupère une activité donnée grâce à son id 
	 * ou l'ensemble des activités de l'utilisateur
	 * 
	 * @param $id id l'activité demandée
	 * @return mixed ??
	 */
	public function get($id = null) {

		$ret = null;

		if($id !== null) {

			$sth = $this->_container['mysql']->prepare(
				'SELECT * FROM training_activity WHERE id=? AND athlete=?'
			);
			$sth->execute([$id, $this->_container['athlete_id']]);
			$ret = $sth->fetch(\PDO::FETCH_OBJ);
			
			if(!$ret) {
				$ret = $this->syncOneWithStrava($id);
			}

			$ret = (object) array_merge((array) $ret, (array) json_decode($ret->raw));

		} else {
			
			$sth = $this->_container['mysql']->prepare(
				'SELECT * FROM training_activity WHERE athlete=?'
			);
			$sth->execute([$this->_container['athlete_id']]);
			$ret = $sth->fetchAll(\PDO::FETCH_OBJ);
			
		}

		return $ret;

	}

	/**
	 * Renvoie la liste des activités filtrée d'après le filtre en entrée
	 * 
	 * @param $filter Filtre respectant le format https://github.com/clue/json-query-language/blob/master/SYNTAX.md#combinators
	 * @deprecated
	 */
	public function filter(array $filter = [], array $options = []) {

		$this->_log->warning('@deprecated filter', ['__METHOD__'=>__METHOD__], ['filter' => $filter]);

		$filter['athlete.id'] = $this->_container['athlete_id'];

		$data = $this->_container['db']->activity->find($filter, $options);

		return $data;

	}

	/**
	 * Renvoie le nombre total d'activité
	 */
	public function count() {
		return $this->_container['mysql']->query(
			'SELECT count(id) FROM training_activity'
		)->fetch(\PDO::FETCH_BOTH)[0];

	}

	/**
	 * Renvoie la liste des activités de type Ride
	 * 
	 * @return array activities
	 */
	public function list() {

		$this->_log->info('list', ['__METHOD__'=>__METHOD__]);

		$sth = $this->_container['mysql']->prepare(
			'SELECT * FROM training_activity WHERE athlete=? AND LOWER(type)="ride"'
		);
		$sth->execute([$this->_container['athlete_id']]);
		$ret = $sth->fetchAll(\PDO::FETCH_OBJ);
		
		return $ret;
	}

	/**
	 * Synchronisation d'une activitié Strava
	 *  Utilisée pour la re-synchronisation d'une activité
	 * 
	 * @param $id Identifiant de l'activité
	 * @return mixed l'activité synchronisée 
	 * @deprecated
	 */
	public function syncOneWithStrava($id) {

		$exchange = 'amq.topic';
		$queue = 'strava-sync.activity';

		$connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
		$channel = $connection->channel();

		/*
			The following code is the same both in the consumer and the producer.
			In this way we are sure we always have a queue to consume from and an
				exchange where to publish messages.
		*/

		/*
			name: $queue
			passive: false
			durable: true // the queue will survive server restarts
			exclusive: false // the queue can be accessed in other channels
			auto_delete: false //the queue won't be deleted once the channel is closed.
		*/
		//$channel->queue_declare($queue, false, true, false, false);

		/*
			name: $exchange
			type: direct
			passive: false
			durable: true // the exchange will survive server restarts
			auto_delete: false //the exchange won't be deleted once the channel is closed.
		*/

		//$channel->exchange_declare($exchange, 'topic', false, true, false);

		//$channel->queue_bind($queue, $exchange);

		$messageBody = json_encode([
			'activity' => ['id' => intval($id)],
			'user' => ['access_token' => utf8_encode($this->_container['access_token'])]
		]);
		$message = new AMQPMessage($messageBody, array(
			'content_type' => 'text/plain', 
			'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
		);
		$channel->basic_publish($message, $exchange, 'strava-sync.user');

		$channel->close();
		$connection->close();

	}

	/**
	 * Synchronsation avec Strava
	 * 
	 * @return array activités
	 */
	public function syncWithStrava() {

		$exchange = 'amq.topic';
		$queue = 'strava-sync.user';

		$connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
		$channel = $connection->channel();

		/*
			The following code is the same both in the consumer and the producer.
			In this way we are sure we always have a queue to consume from and an
				exchange where to publish messages.
		*/

		/*
			name: $queue
			passive: false
			durable: true // the queue will survive server restarts
			exclusive: false // the queue can be accessed in other channels
			auto_delete: false //the queue won't be deleted once the channel is closed.
		*/
		//$channel->queue_declare($queue, false, true, false, false);

		/*
			name: $exchange
			type: direct
			passive: false
			durable: true // the exchange will survive server restarts
			auto_delete: false //the exchange won't be deleted once the channel is closed.
		*/

		//$channel->exchange_declare($exchange, 'topic', false, true, false);

		//$channel->queue_bind($queue, $exchange);

		$messageBody = json_encode([
			'id' => utf8_encode($this->_container['athlete_id']),
			'access_token' => utf8_encode($this->_container['access_token'])
		]);
		$message = new AMQPMessage($messageBody, array(
			'content_type' => 'text/plain', 
			'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
		);
		$channel->basic_publish($message, $exchange, 'strava-sync.user');

		$channel->close();
		$connection->close();

	}

	/**
	 * Calcule la charge TRIMP de l'activité
	 * 
	 * @param id de l'activité
	 * @return string la charge
	 */
	public function actTRIMP($id) {

		$act = $this->get($id);

		if($act === null) {
			return '-';
		}

		if($act->type != 'Ride') {
			return '-';
		}

		if(isset($act->streams->errors)) {
			return '-';
		}

		$stream = $act->streams;

		if(!$stream instanceof \MongoDB\Model\BSONArray ) {
			return '-';
		}

		$customstream = [];
		$max = count($stream[0]->data);
		// Parcours des différents streams demandé
		foreach($stream as $s) {
			$key = $s->type;
			for ($i=0; $i < $max; $i++) {  
				$customstream[$i][$key] = $s->data[$i];
			}
		}

		// Données cardiaques
		$hrmin = 42;
		$hrmax = 196;
		$hrres = $hrmax - $hrmin; // Fréquence cardiaque de réserve

		$pts = json_decode(json_encode($customstream),false);

		$prev = array_shift($pts);
		$cumul = 0;
		$TRIMS = 0;

		foreach($pts as $k=>$pt) {

			if($prev->moving && isset($pt->heartrate)) {

				// Delta de temps entre les 2 mesures
				$dt = $pt->time - $prev->time;

				$cumul += $dt;
				$hr=$pt->heartrate;

				// Calcul TRIMS
				// Calcul de l'instensité
				$TRIMS_int = ($hr-$hrmin)/$hrres;
				$TRIMS += (($dt/60) * $TRIMS_int * (0.64*exp(1.92*$TRIMS_int)));
			}

			$prev = $pt;

		}

		return number_format($TRIMS,2);

	}

	/**
	 * Calcule la charge ESIE de l'activité
	 * 
	 * @param id de l'activité
	 * @return string la charge
	 */
	public function actESIE($id) {

		$act = $this->get($id);
		return isset($act->scores->esie) && is_number($act->scores->esie) ?
			number_format($act->scores->esie, 2) :
			0;
	}

	/**
	 * @deprecated
	 * 
	 * @todo http://www.velo2max.com/blog/lexique/trimps-training-impulse/
	 * @todo http://complementarytraining.net/banister-impulseresponse-model-in-r/
	 */
	public function detail($id) {
		$this->_log->debug('detail, act:'.$id, ['__METHOD__'=>__METHOD__]);

		$ret = [];

		$act = $this->get($id);
		$act = json_decode($act->raw);

		if(!$act || $act->type != 'Ride') {
			$this->_log->warn('detail, act not found or type is not Ride', ['__METHOD__'=>__METHOD__]);
			return $ret;
		}

		$stream = $act->streams;

		if(!is_array($stream)) {
			$this->_log->warn('detail, stream is not an array', ['__METHOD__'=>__METHOD__]);
			var_dump($stream);
			return $ret;
		}

		$customstream = [];
		$max = count($stream[0]->data);
		// Parcours des différents streams demandé
		foreach($stream as $s) {
			$key = $s->type;
			for ($i=0; $i < $max; $i++) {  
				$customstream[$i][$key] = $s->data[$i];
			}
		}

		// Comptage des secondes
		// pour l'échelle ESIE
		$isie = [
			1=>[],
			2=>[],
			3=>[],
			4=>[],
			5=>[],
			6=>[],
			7=>[]
		];

		// Coefficients échelle ESIE
		$coef = [
			1=>2,
			2=>2.5,
			3=>3,
			4=>3.5,
			5=>4.5,
			6=>7,
			7=>11
		];

		// Plages de fréquences cardiaques ESIE
		$limits = [
			1=> ['min'=>0, 'max'=>.75],
			2=> ['min'=>.75, 'max'=>.85],
			3=> ['min'=>.85, 'max'=>.92],
			4=> ['min'=>.92, 'max'=>.96],
			5=> ['min'=>.96, 'max'=>1]
		];

		// Données cardiaques
		$hrmin = 42;
		$hrmax = 196;
		$hrres = $hrmax - $hrmin; // Fréquence cardiaque de réserve

		$pts = json_decode(json_encode($customstream),false);
		// var_dump($pts);

		$prev = reset($pts);
		$cumul = 0;
		$TRIMS = 0;

		foreach($pts as $k=>$pt) {

			//$pt->dt = new DateTime($pt->time);

			if($prev->moving && isset($pt->heartrate)) {

				// Delta de temps entre les 2 mesures
				$dt = $pt->time - $prev->time;

				$cumul += $dt;
				$hr=$pt->heartrate;

				// Calcul ISIE
				$hrpercent = $hr / $hrmax;
				if( $hrpercent <= $limits[1]['max'] )  {
					$m = ($hrpercent*$hrmax) - $hrmin;
					$isie[1]['sommepond'][] = $m * $dt;
					$isie[1]['sommepoids'][] = $dt;
				} elseif ($hrpercent > $limits[2]['min'] && $hrpercent <= $limits[2]['max']) {
					$m = ($limits[2]['max'] - $hrpercent) * $hrmax;
					$isie[2]['sommepond'][] = $m * $dt;
					$isie[2]['sommepoids'][] = $dt;

					$isie[1]['sommepond'][] = 0;
					$isie[1]['sommepoids'][] = $dt;
				} elseif ($hrpercent > $limits[3]['min'] && $hrpercent <= $limits[3]['max']) {
					$isie[3][] += $dt;
				} elseif ($hrpercent > $limits[4]['min'] && $hrpercent <= $limits[4]['max']) {
					$isie[4][] += $dt;
				} elseif ($hrpercent > $limits[2]['min'] && $hrpercent <= $limits[5]['max']) {
					$isie[5][] += $dt;
				} else {
					$isie[7] += $dt;
				}

			}

			$prev = $pt;

		}

		$raw = [];
		$labels = [];
		$ideal = array_fill(0,$cumul, $hrmax * $limits[1]['max']);
		$prev = reset($pts);
		$i=0;
		foreach($pts as $k=>$pt) {

			// Delta de temps entre les 2 mesures
			$dt = $pt->time - $prev->time;

			if($prev->moving && isset($pt->heartrate)) {
				$hr = $pt->heartrate;
			} else {
				$hr = null;
			}

			// Pour chaque seconde on rajoute la valeur calculée
			// for($j=1;$j<$dt;++$j) {
				$labels[] = $pt->time * 1000;
				$raw[] = $hr;
				++$i;
			// }

			$prev = $pt;

		}

		/**
		* calcul ESIE
		*/
		/*$charge_totale = 0;
		foreach($isie as $i=>$v) {
			$charge_totale += ($charge = $coef[$i]*($v/60));
		}*/

		// var_dump($isie);

		#var_dump(array_sum($isie[1]['sommepond']) / array_sum($isie[1]['sommepoids']));
		// var_dump(array_sum($isie[2]['sommepond']) / array_sum($isie[2]['sommepoids']));
		
		// TODO du propre 
		return [
			'act' => $act,
			'labels' => ($labels), 
			'raw' => ($raw), 
			'ideal' => $ideal,
			'moyenne' => array_sum($isie[1]['sommepond']) / array_sum($isie[1]['sommepoids']),
		];
		// exit;

	}



	public function banister() {

		/**
		 * https://www.trainingpeaks.com/blog/the-science-of-the-performance-manager/
		 * https://www.google.fr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwi47srw8P_VAhVHC8AKHZaWBuQQFggyMAE&url=http%3A%2F%2Ffellrnr.com%2Fwiki%2FModeling_Human_Performance&usg=AFQjCNFj_RhN8OKlfx0VQknqd0tUb7Dmmg
		 */

		$fitness = 0;
		$fatigue = 0;
		$TRIMP = $this->getDailyTRIMP(); //array of TRIMP for each day
		$i=0;

		$k1=1.0;
		$k2=2.0; #1.8-2.0, 
		$r1=49; #49-50;
		$r2=11;

		$res = [];

		foreach($TRIMP as $k=>$v) {
			$fitness = $fitness * exp(-1/$r1) + $v;
			$fatigue = $fatigue * exp(-1/$r2) + $v;
			$performance = $fitness * $k1 - $fatigue * $k2;
// var_dump($k);exit;
			$res[$k] = [
				'data' => '[new Date('.date('Y,m,d',strtotime($k)).'),'
					.number_format($fitness,2,'.','').','
					.number_format($fatigue,2,'.','').','
					.number_format($performance,2,'.','').']',
				'fitness' => $fitness,
				'fatigue' => $fatigue,
				'performance' => $performance
			];
		}

// var_dump($res);
		return $res;

	}


	private $dirtyCache;
	private function getDailyTRIMP() {

		if($this->dirtyCache) {
			return $this->dirtyCache;
		}

		/*
		$dbh = new \PDO(
			"pgsql:host=" . getenv("FED_PG_HOST") . ";dbname=" . getenv("FED_PG_DB"),
			getenv("FED_PG_USER"),
			getenv("FED_PG_PASSWORD")
		);
		*/
		$dbh = $this->_container['mysql'];

		$result = $dbh->query("select DATE(start_date) as start_date, count(*), sum(trimp) as trimp" .
		" from training_activity WHERE date(start_date)>='2016-11-01' " .
		" group by date(start_date) order by date(start_date)");

		$indexed_result = [];

		foreach($result as $line) {
			// var_dump($line);exit;
			$indexed_result[$line['start_date']] = intval($line['trimp']);
		}

// var_dump($indexed_result);

		// https://stackoverflow.com/questions/11391085/getting-date-list-in-a-range-in-postgresql
		// En MySQL : https://github.com/gabfl/mysql_generate_series
		$dates = $dbh->query("CALL generate_series_date_day('2016-11-01', DATE(CURDATE()), 1)")->fetchAll(\PDO::FETCH_CLASS);

		$dailyTRIMP = [];
		foreach($dates as $date) {//var_dump($date);exit;
			$d = substr($date->series,0,10);
			$dailyTRIMP[$d] = array_key_exists($d, $indexed_result) ? $indexed_result[$d] : 0;
		}

		$this->dirtyCache = $dailyTRIMP;

		return $dailyTRIMP;

	}

	public function tsb() {

		$atl_today = 0;
		$ctl_today = 0;

		$na = 7;
		$nf = 42;

		$la = 2/($na+1);
		$lf = 2/($nf+1);

		$res = [];

		$TRIMP = $this->getDailyTRIMP();

		foreach($TRIMP as $k => $v) {

			$res[$k] = [
				'data' => '[new Date('.date('Y,m,d',strtotime($k)).'),'
					.number_format($ctl_today,2,'.','').','
					.number_format($atl_today,2,'.','').','
					.number_format($ctl_today-$atl_today,2,'.','').']',
				'fitness' => $ctl_today,
				'fatigue' => $atl_today,
				'performance' => $ctl_today - $atl_today
			];

			$atl_today = $v * $la + ((1-$la) * $atl_today);
			$ctl_today = $v * $lf + ((1-$lf) * $ctl_today);
		}

		return $res;

	}


	public function trimps() {
		$res = [];

		$TRIMP = $this->getDailyTRIMP();

		foreach($TRIMP as $k => $v) {
			$res[$k] = [
				'data' => '[new Date('.date('Y,m,d',strtotime($k)).'),'
					.number_format($v,2,'.','').']',
			];
		}

		return $res;

	}

	public function weekly_trimps() {
		$res = [];

		$TRIMP = $this->getDailyTRIMP();

		$tmp = [];

		foreach($TRIMP as $k => $v) {
			if(!isset($tmp[date('Y_W', strtotime($k))])) {
				$tmp[date('Y_W', strtotime($k))] = 0;
			}
			$tmp[date('Y_W', strtotime($k))] += $v;
		}

		foreach($tmp as $k => $v) {
			$res[$k] = [
				'data' => '["'.$k.'",'
					.number_format($v,2,'.','').']',
			];
		}

		return $res;

	}

	public function monthly_trimps() {
		$res = [];

		$TRIMP = $this->getDailyTRIMP();

		$tmp = [];

		foreach($TRIMP as $k => $v) {
			if(!isset($tmp[date('Y_M', strtotime($k))])) {
				$tmp[date('Y_M', strtotime($k))] = 0;
			}
			$tmp[date('Y_M', strtotime($k))] += $v;
		}

		foreach($tmp as $k => $v) {
			$res[$k] = [
				'data' => '["'.$k.'",'
					.number_format($v,2,'.','').']',
			];
		}

		return $res;

	}

	public function monotony_by_week() {

		$res = [];

		$t = $this->getDailyTRIMP();

		$TRIMP = $this->getDailyTRIMP();

		$tmp = [];

		foreach($TRIMP as $k => $v) {
			if(!isset($tmp[date('Y_W', strtotime($k))])) {
				$tmp[date('Y_W', strtotime($k))] = 0;
			}
			$tmp[date('Y_W', strtotime($k))] += $v;
		}

		foreach($tmp as $k => $v) {
			$res[$k] = [
				'data' => '["'.$k.'",'
					.number_format($v,2,'.','').']',
			];
		}

// select YEARWEEK(date), IFNULL(LEAST(AVG(trimp)/STDDEV(trimp), 10),0) as monotony, IFNULL(AVG(trimp),0) as average, IFNULL(STDDEV(trimp),0) as standarddeviasion from (select date, count(id),  ifnull(sum(trimp),0) as trimp from table2 left join training_activity on table2.date=DATE(training_activity.start_date) group by date order by date) as t GROUP BY YEARWEEK(date) order by yearweek (date);
//select YEARWEEK(date), IFNULL(LEAST(AVG(trimp)/STDDEV(trimp), 10),0) as monotony, IFNULL(AVG(trimp),0) as average, IFNULL(STDDEV(trimp),0) as standarddeviasion, IFNULL(SUM(trimp)/IFNULL(LEA ST(AVG(trimp)/STDDEV(trimp), 10),0),0) as strain from (select date, count(id), ifnull(sum(trimp),0) as trimp from table2 left join training_activity on table2.date=DATE(training_activity.start_dat e) group by date order by date) as t GROUP BY YEARWEEK(date) order by yearweek(date);

		return $res;
		
	}

}

/*

CREATE TEMPORARY TABLE IF NOT EXISTS table2 (date DATE) ENGINE=MEMORY;
CALL generate_series_date_day('2016-11-01', DATE(CURDATE()),1);
insert into table2 SELECT series FROM series_tmp;
select date, count(id), ifnull(sum(trimp),0) as TRIMP from table2 left join training_activity on table2.date=DATE(training_activity.start_date) group by date order by date;


select YEARWEEK(date), IFNULL(LEAST(AVG(trimp)/STDDEV(trimp), 10),0) as monotony, IFNULL(AVG(trimp),0) as average, IFNULL(STDDEV(trimp),0) as standarddeviasion, IFNULL(SUM(trimp)/IFNULL(LEA ST(AVG(trimp)/STDDEV(trimp), 10),0),0) as strain from (select date, count(id), ifnull(sum(trimp),0) as trimp from table2 left join training_activity on table2.date=DATE(training_activity.start_dat e) group by date order by date) as t GROUP BY YEARWEEK(date) order by yearweek(date);
*/

// https://developer.mozilla.org/fr/docs/Web/SVG/Tutoriel/Paths#Commandes_de_courbes