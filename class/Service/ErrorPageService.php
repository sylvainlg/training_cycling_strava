<?php
namespace SylvainLG\Training\Service;

class ErrorPageService extends \SylvainLG\Training\AbstractService {

	public function __construct($c) {
		parent::__construct($c);
	}

	public function hey400() {
		// header('HTTP/1.1 400 Bad request');
		return $this->render('error/400.html.twig', ['errCode'=>400, 'errMessage'=>'Bad Request']);
	}

	public function hey404() {
		// header('HTTP/1.1 404 Not found');
		return $this->render('error/404.html.twig', ['errCode'=>404, 'errMessage'=>'Not Found']);
	}



	private function render($view, $parameters) {
		return $this->_container['twig']->render($view, $parameters);
	}

}