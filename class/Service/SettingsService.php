<?php

namespace SylvainLG\Training\Config;

class SettingsService extends \SylvainLG\Training\AbstractService {

    private static $_DBFILE = APPDIR . '/db/conf/conf.json';

    private $settings = [];

    private $_modified = false;

    /**
    * Le constrcuteur charge la configuration depuis le fichier json
    **/
    public function __construct($c) {
		parent::__construct($c);
        $this->settings = json_decode(file_get_contents(self::$_DBFILE), true);
    }

    /**
    *  Permet d'obtenir la valeur de la configuration
    *  @param $key string clef à récupérer
    *  @return mixed
    **/
    public function get($key)
    {
        $this->_log->debug('config :: get $key:' . $key, ['__METHOD__'=>__METHOD__]);
        if (!isset($this->settings[$key])) {
            $this->_log->debug('config :: get $key:' . $key . ' not found', ['__METHOD__'=>__METHOD__]);
            return null;
        }
        return $this->settings[$key];
    }

    /**
     * Permet d'obtenir l'ensemble de la configuration sous forme de tableau
     * @return array
     */
    public function getAll() {
        $this->_log->debug('config :: getAll', ['__METHOD__'=>__METHOD__]);
        return $this->settings;
    }

    /**
    *  Permet de modifier la valeur de la configuration
    *  @param $key string clef à modifier
    *  @param $value mixed valeur à configurer
    *  @return ConfigService $this
    **/
    public function set($key, $value)
    {
        $this->_log->debug('config :: set:'.$key, ['__METHOD__'=>__METHOD__]);
        $this->settings[$key] = $value;
        $this->_modified = true;
        return $this;
    }

    /**
     * Implémentation de la fonction __destruct qui va sauvegarder le fichier de configuration
     */
    public function __destruct() {

        if($this->_modified) {

            $this->_log->debug('config :: config is modified, saving', ['__METHOD__'=>__METHOD__]);
            
            if(FALSE===file_put_contents(self::$_DBFILE ,json_encode($this->settings))) {
                $this->_log->error('config :: error on save configuration', ['__METHOD__'=>__METHOD__]);
            }

        }

    }

}

