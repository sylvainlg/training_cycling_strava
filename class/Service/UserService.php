<?php

namespace SylvainLG\Training\Service;

/**
 * Service gérant les utilisateurs
 */
class UserService extends \SylvainLG\Training\AbstractService {

	/**
	 * Sauvegarde le jeton pour accéder à Strava.
	 * Ceci permet de faire les synchronisations sans que l'utilisateur les lance
	 * à la main.
	 * 
	 * @param int $id Identifiant Strava de l'utilisateur
	 * @param string $access_token Le jeton attribué à l'utilisateur
	 */
	public function storeAccessToken($id, $access_token) {
		
		$this->_log->debug('store access token', ['__METHOD__'=>__METHOD__, 'user' => $id]);

		$db = $this->_container['mysql'];
		$sth = $db->prepare('INSERT INTO training_user(id, access_token) ' . 
		'VALUES (:id,:accesstoken) ON DUPLICATE KEY UPDATE access_token = :accesstoken');
		$res = $sth->execute([
			':id' => $id,
			':accesstoken' => $access_token
		]);

		if(!$res) {
			$this->_log->error('Access token cannot be stored', 
				['__METHOD__'=>__METHOD__, 'user' => $id, 'error' => $sth->errorInfo()]);
		} else {
			$this->_log->info('Access token stored', ['__METHOD__'=>__METHOD__, 'user' => $id]);
		}

	}

	/**
	 * @deprecated
	 */
	public function getZones() {
		return $this->_container['api']->get('athlete/zones');
	}
	
}