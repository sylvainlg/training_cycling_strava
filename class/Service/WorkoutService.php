<?php

namespace SylvainLG\Training\Service;

use \SylvainLG\Training\Model\Workout;

/**
 * Class Workout
 *
 * Permet de manipuler les entraînements
 */

class WorkoutService extends \SylvainLG\Training\AbstractService {

	/**
	 * Coefficients EISE
	 */
	private $coef = [
		'i1'=>2,
		'i2'=>2.5,
		'i3'=>3,
		'i4'=>3.5,
		'i5'=>4.5,
		'i6'=>7,
		'i7'=>11
	];

	private $debug = false;

	/**
	 * Constructeur
	 */
	public function __construct($c) {
		parent::__construct($c);
	}

	/**
	 * Retourne tous les entraînements
	 */
	public function all() {
		$this->_log->info('all', ['__METHOD__'=>__METHOD__, 'filter'=>['athlete.id' => $this->_container['athlete_id']]]);
		//return $this->_container['db']->workout->find(['athlete.id' => $this->_container['athlete_id']], ['sort'=> ['date'=>-1]]);
		return $this->_container['mysql']->query('SELECT * FROM workout ORDER BY date DESC')->fetchAll();
	}

	/**
	 * Retourne l'entraînement voulu
	 * 
	 * @return mixed workout or null
	 */
	public function get($uuid) {
		$this->_log->info('get '. $uuid, ['__METHOD__'=>__METHOD__]);
		
		$sth = $this->_container['mysql']->prepare(
			'SELECT * FROM workout WHERE uuid=? AND athlete=? LIMIT 1');
		$sth->execute([
			$uuid,
			$this->_container['athlete_id']
		]);

		return $sth->fetchObject(Workout::class, ['hydrate'=>true]) ?? null;
	}

	/**
	 * Renvoie la liste des activités
	 * 
	 * @param $filter Filtre respectant le format https://github.com/clue/json-query-language/blob/master/SYNTAX.md#combinators
	 */
	 public function filter(array $filter = [], array $options = []) {

		$this->_log->info('get', ['__METHOD__'=>__METHOD__, 'filter' => $filter]);
		throw new Exception("@deprecated");
		$filter['athlete.id'] = $this->_container['athlete_id'];
		$data = $this->_container['db']->workout->find($filter, $options);
		return $data;

	}

	/**
	 * 
	 */
	public function count(array $filter = [], array $options = []) {

		$this->_log->info('get', ['__METHOD__'=>__METHOD__, 'filter' => $filter]);
		throw new Exception("@deprecated");
		$filter['athlete.id'] = $this->_container['athlete_id'];
		$data = $this->_container['db']->workout->count($filter, $options);
		return $data;

	}


	/**
	 * Ajoute un entraînement à la liste
	 *
	 * @param \SylvainLG\Training\Model\Workout workout 
	 * @return this
	 */
	public function add(Workout $w) {
		$this->_log->info('add', ['__METHOD__'=>__METHOD__, $w]);
		
		
		$w->athlete = intval($this->_container['athlete_id']);

		$sth = $this->_container['mysql']->prepare(
			'INSERT INTO workout(uuid, title, date, type, content, notes, duration, difficulty, activityId, steps, athlete) '.
			' VALUES(?,?,?,?,?,?,?,?,?,?,?)'
		);
		
		$res = $sth->execute([
			$w->uuid,
			$w->title,
			$w->date,
			$w->type,
			$w->content ?? '',
			$w->notes ?? NULL,
			$w->duration ?? NULL,
			$w->difficulty ?? NULL,
			$w->activityId ?? NULL,
			json_encode($w->steps) ?? NULL,
			$w->athlete
		]);

		if(!$res) {
			$this->_log->error('Cannot store workout', ['__METHOD__'=>__METHOD__, $w,  $sth->errorInfo()]);
			throw new \Exception('Cannot store workout');
		}

		return $this;
	}

	/**
	 * Modification d'un entraînement
	 */
	public function set(Workout $w) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $w]);
		
		$w->athlete = $this->_container['athlete_id'];

		$sth = $this->_container['mysql']->prepare(
			'UPDATE workout SET title=?, date=?, type=?, content=?, notes=?, duration=?, difficulty=?, activityId=?, steps=?'.
			' WHERE uuid=? AND athlete=?'
		);
		$res = $sth->execute([
			$w->title,
			$w->date,
			$w->type,
			$w->content,
			$w->notes,
			$w->duration,
			$w->difficulty,
			$w->activityId,
			json_encode($w->steps),
			$w->uuid,
			$w->athlete
		]);

		if(!$res) {
			$this->_log->error('Cannot store workout', ['__METHOD__'=>__METHOD__, $w]);
			throw new Excetion('Impossible de mettre à jour le Workout');
		}

		return $this;

	}

	/**
	 * Suppression d'un workout
	 * 
	 * @param Workout
	 * @return boolean
	 */
	public function delete(Workout $w) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $w]);

		$sth = $this->_container['mysql']->prepare(
			'DELETE FROM workout WHERE athlete=? AND uuid=?'
		);
		$res = $sth->execute([
			$this->_container['athlete_id'],
			$w->uuid
		]);

		if(!$res) {
			throw new \Excetion('Impossible de supprimer le Workout');
		}

		return $this;
	}


	public function today() {
		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type, title, content FROM workout WHERE DATE(date) = CURDATE()');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive today workouts', ['__METHOD__'=>__METHOD__]);
			return [];
		}
		return $sth->fetchAll();
	}

	
	public function tomorrow() {
		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type, date, content, title FROM workout WHERE DATE(date) = CURDATE() + INTERVAL 1 DAY');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive tomorrow workouts', ['__METHOD__'=>__METHOD__]);
			return [];
		}
		return $sth->fetchAll();
	}

	public function todo() {
		/*
		'date' => ['$lte' => new \MongoDB\BSON\UTCDateTime(time()*1000)],
			'notes' => ['$eq' => ''],
			'duration' => ['$eq' => ''],
			'difficulty' => ['$eq' => ''],
			'activityId' => ['$eq' => null],
		*/
		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type, title, date FROM workout WHERE date <= NOW() AND '.
			'NULLIF(notes,"") IS NULL AND NULLIF(duration,"") IS NULL AND NULLIF(difficulty,"") IS NULL AND activityId IS NULL');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive tomorrow workouts', ['__METHOD__'=>__METHOD__]);
			return [];
		}
		return $sth->fetchAll();
	}

	public function noStrava() {
		
		$sth = $this->_container['mysql']->query(
			'SELECT uuid, type, title, date FROM workout WHERE date <= NOW() AND activityId IS NULL');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive tomorrow workouts', ['__METHOD__'=>__METHOD__]);
			return [];
		}
		return $sth->fetchAll();
	}

	public function week() {
		
		$sth = $this->_container['mysql']->query(
			'SELECT * FROM workout WHERE YEARWEEK(date,1) = YEARWEEK(CURDATE(),1) ORDER BY date DESC');
		if(!$sth) {
			$this->_container['log']->error('Could not retrive week workouts', ['__METHOD__'=>__METHOD__]);
			return [];
		}
		return $sth->fetchAll(\PDO::FETCH_CLASS, Workout::class, ['hydrate' => true]);
	}
	
	/**
	 * Calcul de la charge théorique de l'entrainement ESIE
	 */
	public function esie(Workout $w) {
	
		// $this->debug = $w->uuid == 'workout_5857054f061d58.53788979';

		if($w->type !== 'ride') {
			return 0;
		}

		$duration = 0;
		$val = 0;
		// if($w->duration) {
			$duration = 0;//strtotime("1970-01-01 $w->duration")+3600;
			if($w->steps) {
				$val += $this->calculEsie($duration, $w->steps);
			}

		// } // BRAIN FUCK ERROR => GoTo bed;

if($this->debug) var_dump($val);

		$this->debug = false;

		return $val;

	}

	private function calculEsie(&$duration, $steps) {

		$val = 0;
		// TODO => clean this up
		if(!is_array($steps)) return 0;
		foreach($steps as $step) {
			if($step->type == 'repetition') {
				$val += $this->calculEsie($duration, $step->steps);
			} else {

				// Do the math stuff

				if($step->completion == 'duration' && property_exists($step, 'duration')) {
if($this->debug) var_dump($step);
					$dur = (strtotime("1970-01-01 $step->duration")+3600);
					$duration -= $dur;
					if($step->intensity !== '-') { 
						$val += $this->coef[$step->intensity ?? 'i1'] * ($dur/60);
					}
					// exit;
				}
if($this->debug) var_dump($val);
			}

		}
		return $val;

	}

	public function realesie($w) {
		$val = 0;
		if(isset($w->activityId) && $w->activityId !== null) {
			$val = intval($this->_container['activity']->actESIE($w->activityId));

		}
		return $val;
	}




	/**
	 * Calcule la charge ESIE de l'activité
	 * 
	 * @param id de l'activité
	 * @return string la charge
	 */
	public function actESIE($id) {

		// Données cardiaques
		$hrmin = 42;
		$hrmax = 196;
		$hrres = $hrmax - $hrmin; // Fréquence cardiaque de réserve

		$pts = json_decode(json_encode($customstream),false);
		// var_dump($pts);

		$prev = array_shift($pts);
		$cumul = 0;
		$TRIMS = 0;

		foreach($pts as $k=>$pt) {

			//$pt->dt = new DateTime($pt->time);

			if($prev->moving && isset($pt->heartrate)) {

				// Delta de temps entre les 2 mesures
				$dt = $pt->time - $prev->time;

				$cumul += $dt;
				$hr=$pt->heartrate;

				// Calcul ISIE
				$hrpercent = $hr / $hrmax;
				if( $hrpercent <= $limits[1]['max'] )  {
					$isie[1] += $dt;
				} elseif ($hrpercent > $limits[2]['min'] && $hrpercent <= $limits[2]['max']) {
					$isie[2] += $dt;
				} elseif ($hrpercent > $limits[3]['min'] && $hrpercent <= $limits[3]['max']) {
					$isie[3] += $dt;
				} elseif ($hrpercent > $limits[4]['min'] && $hrpercent <= $limits[4]['max']) {
					$isie[4] += $dt;
				} elseif ($hrpercent > $limits[2]['min'] && $hrpercent <= $limits[5]['max']) {
					$isie[5] += $dt;
				} else {
					$isie[7] += $dt;
				}

			}

			$prev = $pt;

		}

		/**
		* calcul ESIE
		*/
		$charge_totale = 0;
		foreach($isie as $i=>$v) {
			$charge_totale += ($charge = $coef[$i]*($v/60));
		}

		return number_format($charge_totale,2);
	}

}

