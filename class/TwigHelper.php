<?php

namespace SylvainLG\Training;

/**
 * Fonctions utilisable dans Twig
 *  Configuré : helper.function() dans index.php
 */
class TwigHelper implements ContainerAwareInterface {

	use ContainerAwareTrait;

	/**
	 * @deprecated
	 */
	public function act_trimp($id) {
		return $this->_container['activity']->actTRIMP($id);
	}

	/**
	 * @deprecated
	 */
	public function act_esie($id) {
		return $this->_container['activity']->actESIE($id);
	}

	public function gmdate($format,$timestamp) {
		return gmdate($format,$timestamp);
	}

	public function ucfirst($text) {
		return ucfirst($text);
	}

}