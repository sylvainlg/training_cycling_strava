#!/usr/bin/env bash

if [ ! -f /home/bas/applicationrc ]; then
	exit 0;
fi

# source environment variables
# https://www.clever-cloud.com/doc/admin-console/environment-variables/
source /home/bas/applicationrc

echo "Run npm install && bower install"
npm install
#ls -la node_modules
#ls -la node_modules/bower
node_modules/bower/bin/bower install