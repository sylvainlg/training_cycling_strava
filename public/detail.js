(function (data){
	'use strict';

	moment.locale('fr');

	// Chart.default.global.responsive = false;
console.log(data);
	var ctx = document.getElementById("myChart");

	var data = {
		// labels: ["January", "February", "March", "April", "May", "June", "July"],
		labels: data.labels,
		datasets: [
			{
				label: "Activity data",
				fill: false,
				lineTension: 0.1,
				backgroundColor: "rgba(75,192,192,0.4)",
				borderColor: "rgba(75,192,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: data.raw,
				// spanGaps: false,
			},
			{
				label: "Perfect activity",
				data: data.ideal,
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(75,192,3,0.4)",
				borderColor: "rgba(75,192,3,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 1,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 1,
				pointRadius: 1,
				pointHitRadius: 10,

			}
		]
	};


	var myChart = new Chart(ctx, {
		type: 'line',
		data: data,
        options: {
			scales: {
				yAxes: [{
					/*ticks: {
						beginAtZero:true
					},*/
				}],
				xAxes: [{
					type: "time",
					scaleLabel: {
						display: true,
						labelString: 'Hour'
					},
					time: {
						displayFormats: {
							hour: 'hh:mm:ss'
						}
					}
				}]
			}
		}
	});


})(data);