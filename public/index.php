<?php
// Start user session
session_start();

define('APP_ENV', (php_sapi_name() == 'cli-server') ? 'dev' : getenv('APP_ENV') ?? 'unknow');
define('APPDIR', realpath(__DIR__.DIRECTORY_SEPARATOR.'..'));

if(!getenv('MONGODB_ADDON_URI')) {
    putenv('MONGODB_ADDON_DB=training');
    putenv('MONGODB_ADDON_URI=mongodb://192.168.1.22/training');
}

// Include mandatory files
// and register autoloads
require APPDIR.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

use \Monolog\Handler\ErrorLogHandler;
use \Monolog\Logger;

// Create Service Container
use Pimple\Container;
$container = new Container();
$container->register(new \SylvainLG\Training\PimpleProvider());

if(array_key_exists('strava', $_SESSION)) {
    $container['access_token'] = $_SESSION['strava']->access_token;
    $container['athlete_id'] = intval($_SESSION['strava']->athlete->id);
}

$container['log'] = function($c) {
    // create a log channel
    $loglevel = $c['config']->get('loglevel') ?? 'error';

    $log = new Logger('default');
    $handler = new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::toMonologLevel($loglevel));
    $handler->setFormatter(new \Bramus\Monolog\Formatter\ColoredLineFormatter());
    $log->pushHandler($handler);
    return $log;
};

$container['twig'] = function($c) {

    $loader = new Twig_Loader_Filesystem(APPDIR . '/templates');
    
    $twigHelper = new \SylvainLG\Training\TwigHelper();
    $twigHelper->setContainer($c);
    
    $twigExtension = new \SylvainLG\Training\TwigExtension();
    $twigExtension->setContainer($c);

    $twig = new Twig_Environment($loader, array(
        // 'cache' => APPDIR . '/twig_cache',
        'cache' => false,
        'debug' => APP_ENV == 'dev',
    ));
    $twig->addGlobal('helper', $twigHelper);
    $twig->addExtension($twigExtension);
    if(APP_ENV == 'dev') {
        $twig->addExtension(new Twig_Extension_Debug());
    }

    return $twig;
};

$container['errorpage'] = function($c) {
    return new \SylvainLG\Training\Service\ErrorPageService($c);
};

$container['accept'] = function($c) {
    return new \Trii\HTTPHeaders\Accept($_SERVER['HTTP_ACCEPT']);
};

$container['mysql'] = function($c) {
    return new PDO(getenv('RPI_MYSQL_DSN'), getenv('RPI_MYSQL_USER'), getenv('RPI_MYSQL_PWD'));
};

$container['user'] = function($c) {
    return new \SylvainLG\Training\Service\UserService($c);
};

/*** Router section *************/

$router = $container['router'];
$router->get(['/login', 'login'], ['SylvainLG\Training\Controller\LoginController', 'index']);
$router->get(['/token_exchange', 'token_exchange'], ['SylvainLG\Training\Controller\LoginController', 'tokenExchange']);

$router->filter('auth', function() use ($router) {    
    if(!isset($_SESSION['strava']->access_token)) 
    {
        header('Location: ' . $router->route('login', []));

        return false;
    }
});

$router->group(['before' => 'auth'], function($router) {

    $router->any(['/', 'home'], ['SylvainLG\Training\Controller\HomeController','index']);
    $router->group(['prefix'=>'home'], function($router) {
        $router->get(['/events', 'home_event'], ['SylvainLG\Training\Controller\HomeController', 'events']);
        $router->post(['/events', 'home_event'], ['SylvainLG\Training\Controller\HomeController', 'events']);
        $router->get(['/me', 'home_me'], ['SylvainLG\Training\Controller\HomeController', 'me']);
        $router->get(['/forme', 'home_forme'], ['SylvainLG\Training\Controller\HomeController', 'forme']);
        $router->get(['/prout', 'home_prout'], ['SylvainLG\Training\Controller\HomeController', 'prout']);
    });

    $router->get(['/calendar', 'calendar'], ['SylvainLG\Training\Controller\PlanningController', 'index']);
    $router->get(['/planning', 'planning'], ['SylvainLG\Training\Controller\PlanningController', 'index']);

    $router->get(['/logout', 'logout'], ['SylvainLG\Training\Controller\LoginController', 'logout']);

    $router->any(['/settings', 'settings'], ['SylvainLG\Training\Controller\SettingsController','index']);

    $router->group(['prefix'=>'activity'], function($router) {
        $router->get(['/sync', 'activity_sync'], ['SylvainLG\Training\Controller\ActivityController', 'sync']);
        $router->get(['/list', 'activity_list'], ['SylvainLG\Training\Controller\ActivityController', 'list']);
        // $router->get('/list1', ['SylvainLG\Training\Controller\ActivityController', 'list1']);
        // $router->get('/list2', ['SylvainLG\Training\Controller\ActivityController', 'list2']);
        $router->get(['/detail/{id:\d+}', 'activity_detail'], ['SylvainLG\Training\Controller\ActivityController', 'detail']);
        $router->get(['/charts', 'activity_charts'], ['SylvainLG\Training\Controller\ActivityController', 'charts']);
    });

    $router->group(['prefix'=>'workout'], function($router) {
        $router->get(['/', 'workout'], ['SylvainLG\Training\Controller\WorkoutController', 'index']);
        $router->get(['/detail/{uuid:[a-z0-9_-]+}', 'workout_detail'], ['SylvainLG\Training\Controller\WorkoutController', 'detail']);
        $router->get(['/match/{uuid}/{activityId}', 'workout_match'], ['SylvainLG\Training\Controller\WorkoutController', 'match']);
        $router->get(['/match_validate/{uuid}/{activityId}', 'workout_match_validate'], ['SylvainLG\Training\Controller\WorkoutController', 'matchValidate']);
        $router->get(['/workout_to_activity/{uuid}', 'workout_to_activity'], ['SylvainLG\Training\Controller\WorkoutController', 'workoutToActivity']);
        $router->post(['/workout_to_activity/{uuid}', 'workout_to_activity'], ['SylvainLG\Training\Controller\WorkoutController', 'workoutToActivity']);
        $router->get(['/list', 'workout_list'], ['SylvainLG\Training\Controller\WorkoutController', 'list']);
        $router->get(['/creator', 'workout_creator'], ['SylvainLG\Training\Controller\WorkoutController', 'creator']);
        $router->get(['/creator_ride/{id}?', 'workout_creator_ride'], ['SylvainLG\Training\Controller\WorkoutController', 'creatorRide']);
        $router->get(['/creator_workout/{id}?', 'workout_creator_workout'], ['SylvainLG\Training\Controller\WorkoutController', 'creatorWorkout']);
        $router->post(['/add', 'workout_add'], ['SylvainLG\Training\Controller\WorkoutController', 'add']);
        $router->get(['/edit/{id}', 'workout_edit'], ['SylvainLG\Training\Controller\WorkoutController', 'edit']);
        $router->post(['/edit/{id}', 'workout_edit'], ['SylvainLG\Training\Controller\WorkoutController', 'edit']);
        $router->get(['/delete/{id}', 'workout_delete'], ['SylvainLG\Training\Controller\WorkoutController', 'delete']);
        $router->get(['/unmatch/{id}', 'workout_unmatch'], ['SylvainLG\Training\Controller\WorkoutController', 'unmatch']);
    });

    $router->group(['prefix' => 'api/v1/'], function($router) {
        // On ne peut pas nommer une route API (controller)
        // $router->controller(['workout', 'api_workout'], 'SylvainLG\Training\Controller\Api\WorkoutRestController');
        $router->controller('workout', 'SylvainLG\Training\Controller\Api\WorkoutRestController');
        $router->controller('planning', 'SylvainLG\Training\Controller\Api\PlanningRestController');
        $router->controller('fuck', 'SylvainLG\Training\Controller\Api\FuckRestController');
    });
    

    $router->get(['/lab', 'lab'], ['SylvainLG\Training\Controller\LabController', 'index']);
    $router->get(['/lab/mongo', 'lab_mongo'], ['SylvainLG\Training\Controller\LabController', 'mongo']);

});

$router->get(['css', 'css'], function() {
    //$container['log']->info('Serve static file : ' . parse_url($_SERVER['REQUEST_URI'])['path'], ['main']);
    $directory = '../sass';
    \Leafo\ScssPhp\Server::serveFrom($directory);

});

/*********************************
**** Application start here ******
*********************************/

$response = '';

/**
 * If we request a real file, serve it !
 * This is usualy the work of the web server but php -S don't handle it
 */
$f = realpath(APPDIR . '/public/' . parse_url($_SERVER['REQUEST_URI'])['path']);
if(is_file($f)) {
    $container['log']->info('Serve static file : ' . parse_url($_SERVER['REQUEST_URI'])['path'], ['main']);
    $response = file_get_contents($f);
} else {
    try {
        $resolver = new SylvainLG\Training\RouterResolver($container);
        $dispatcher = new Phroute\Phroute\Dispatcher($router->getData(), $resolver);
        $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    } catch(\Phroute\Phroute\Exception\HttpMethodNotAllowedException $e) {
        // throw $e;
        header('HTTP/1.1 405 Method Not Allowed');
        $container['log']->error($e->getMessage(), ['main']);
        if(defined('APP_ENV') && APP_ENV === 'dev') {
            echo $e->getMessage();
        } else {
            header('HTTP/1.1 500 Internal Error');
        }
        exit;
    } catch(\Phroute\Phroute\Exception\HttpRouteNotFoundException $e) {
        header('HTTP/1.1 404 Lost memory, don\'t find the path anyway');
        $container['log']->error($e->getMessage(), ['main']);
        if(defined('APP_ENV') && APP_ENV === 'dev') {
            echo $e->getMessage();
        } else {
            header('HTTP/1.1 500 Internal Error');
        }
        exit;
    } catch(\Exception $e) {
        header('HTTP/1.1 404 Lost memory, don\'t find the path anyway');
        $container['log']->critical($e->getMessage(), ['main']);
        $container['log']->critical($e, ['main']);
        if(defined('APP_ENV') && APP_ENV === 'dev') {
            echo '<pre>' . $e->getTraceAsString() . '</pre>';
            echo PHP_EOL;
            echo $e->getMessage();
        } else {
            header('HTTP/1.1 500 Internal Error');
        }
        exit;
    }
}

// Print out the value returned from the dispatched function
echo $response;
