(function() {
	'use strict';

	angular.module('workoutCreator', ['ngRoute'])
		.config(['$routeProvider', WorkoutCreatorConfig])
		.run(['$rootScope', WorkoutCreatorRun])
		.controller('WorkoutCreator', ['$scope', WorkoutCreator])
		.controller('WorkoutCreatorRide', ['$scope', '$http', WorkoutCreatorRide])
		.controller('WorkoutCreatorPpg', ['$scope', '$http', WorkoutCreatorPpg])
		.directive('workout', workoutDirective)
		.filter('debug', function() {
			return function(input) {
				if (input === '') return 'empty string';
				return input ? input : ('' + input);
			};
		})
	;

function WorkoutCreatorConfig($routeProvider) {

	$routeProvider
		.when('/', {
			controller: 'WorkoutCreator',
			controllerAs: 'wc',
			templateUrl: '/ng-templates/workout/creator.html'
		})
		.when('/ride', {
			controller: 'WorkoutCreatorRide',
			controllerAs: 'wcr',
			templateUrl: '/ng-templates/workout/creator-ride.html'
		})
		.when('/ppg', {
			controller: 'WorkoutCreatorPpg',
			controllerAs: 'wcp',
			templateUrl: '/ng-templates/workout/creator-ppg.html'
		});

}

function WorkoutCreatorRun($rootScope) {
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
		console.log('State change to', toState);
    });
}

function WorkoutCreator($scope) {
	var vm = this;

	$scope.type;
	
	vm.change = function() {
		console.log('WorkoutCreator:change', $scope.type);
		if($scope.type == 'ride') {
			window.location.hash = '/ride';
		}
	}

	vm.setType = function(type) {
		if(type == 'ride') {
			window.location.hash = '/ride';
		} else if(type == 'ppg') {
			window.location.hash = '/ppg';
		}
	}

}

function WorkoutCreatorRide($scope, $http) {

	console.log('WorkoutCreatorRide call');
	var that = this;
	
	$scope.data = [
		[],
	];

	that.meta = {
		type: 'ride',
	};

	that.steps = [
		{
			type:'step', 
			intensity: '-',
			durationHtml: null,
			completion: '-'
		}
	];
	// updateCanvas();
	
	dumpJSON();

	that.addStep = function(parent) {

		console.log(parent);
		if(parent == null) {
			console.log('FUCK OFF');
			return;
		}

		parent.push({
			type:'step', 
			intensity: '-',
			durationHtml: null,
			completion: '-'});

		// updateCanvas();
		dumpJSON();
	};

	that.addRepetition = function(parent) {

		console.log(parent);

		parent.push({
			type:'repetition', 
			intensity: 'i1',
			steps: [{
				type:'step', 
				intensity: '-',
				durationHtml: null,
				completion: '-'
			}],
			duration: null,
			completion: '-'});
		
		// updateCanvas();
		dumpJSON();
	};

	that.describe = function(step) {
		if(step.type=='step') {
			return 'Libre';
		}
	}

	that.save = function() {
		dumpJSON();

		if(that.meta.title
			&& that.meta.date) {
			
			let steps = that.steps;

			for(let step of steps) {
				step.duration = moment(step.durationHtml).format('HH:mm:ss');
			}

			let req = {
				method: 'POST',
				url: '/api/v1/workout',
				headers: {
					'Content-Type': 'application/json'
				},
				data: { 
					title: that.meta.title,
					type: 'ride',
					date: moment(that.meta.date).format('YYYY-MM-DD'),
					content: that.meta.content,
					steps: that.steps
				 }
			};

			console.log(req);

			$http(req)
			 .then(function(result) {
				 console.log('success', result);
			 }, function(error) {
				 console.log('error', error);
			 });

		} else {
			alert('Titre et date obligatoires');
		}

		return;
	}

	function countSteps(steps) {
		let count = 0;
		// if(!steps) {
		// 	return 0;
		// }
		for(let step of steps) {
			if(step.type=='step') {
				++count;
			} else if( step.type == 'repetition') {
				count = count + countSteps(step.steps);
			}
		}
		return count;
	}

	function dumpJSON() {
		console.log(that.steps);
	}

	function updateCanvas() {
		var g = document.getElementById('graph-ride');
		var ctx = g.getContext('2d');
		ctx.clear();

		/*ctx.fillStyle = "rgb(200,0,0)";
        ctx.fillRect (10, 10, 55, 50);

        ctx.fillStyle = "rgba(0, 0, 200, 0.5)";
        ctx.fillRect (30, 30, 55, 50);*/

		var minHeight = 20;
		var maxHeight = g.height-20;

		var minWidth = 20;
		var maxWidth = g.width - 20;

		ctx.beginPath();
		ctx.moveTo(minWidth, minHeight);
		ctx.lineTo(minWidth, maxHeight);
		ctx.lineTo(maxWidth, maxHeight);
		ctx.strokeStyle="rgba(150,150,150)";
		ctx.stroke();

		ctx.fillText('0', 10, maxHeight);
		ctx.fillText('?', g.width-30, g.height-8);
		ctx.fillText('200', 10, 10);

		let ctxinfo = {minHeight: 20,
				maxHeight: g.height-20,
				minWidth: 20,
				maxWidth: g.width - 20,
				maxHr: isie('i7'),
				ampHeight: maxHeight-minHeight,
				ampWidth: maxWidth-minWidth
		};
		let counter = {
			nb: countSteps(that.steps),
			count: 0
		};

		drawSteps(ctx, ctxinfo, counter, that.steps);

		

		// ctx.beginPath()

		// ctx.moveTo(minWidth, val);
		// ctx.lineTo(maxWidth, val);
		// ctx.fill();

	}

	function drawSteps(ctx, ctxinfo, counter, steps) {

		for(let step of steps) {

			if(step.type == 'step') {

				var val = ( (isie(step.intensity) * ctxinfo.ampHeight) / ctxinfo.maxHr );
		console.log(val);

				ctx.fillStyle = "rgb(0,0,0)";
				ctx.fillText(isie(step.intensity), 2, ctxinfo.maxHeight - val + 5);
				ctx.beginPath();
				ctx.moveTo(18, ctxinfo.maxHeight - val);
				ctx.strokeStyle="rgba(0,0,0)";
				ctx.lineTo(20, ctxinfo.maxHeight - val);
				ctx.stroke();

				ctx.fillStyle = "rgba(0, 0, 200, 0.5)";
console.log('Draw Rect : ' + (ctxinfo.minWidth + ( (ctxinfo.ampWidth / counter.nb) * counter.count ) ) +","+ (ctxinfo.maxHeight - val) +","+ (ctxinfo.maxWidth - ctxinfo.minWidth) / counter.nb+","+ val)
console.log(ctx.fillStyle);
				var x = ctxinfo.minWidth + (ctxinfo.ampWidth / counter.nb) * counter.count;
				var y = ctxinfo.maxHeight - val;
				var width = (ctxinfo.maxWidth - ctxinfo.minWidth) / counter.nb;
				var height = val;
				ctx.fillRect( x, y, width, height );

				ctx.strokeStyle = "rgba(0, 0, 200, 1)";
				ctx.beginPath();
				ctx.moveTo(x, y);
				ctx.lineTo(x+width, y);
				ctx.stroke();


				counter.count++;
			} else {
				drawSteps(ctx, ctxinfo, counter, step.steps);
			}

		}

		
	}


	function isie(k) {
	let isie = {
			i1: 140,
			i2: 160,
			i3: 170,
			i4: 180,
			i5: 190,
			i6: 196,
			i7: 200
		};
		return isie[k];
	}

}

function WorkoutCreatorPpg($scope, $http) {

	console.log('WorkoutCreatorPpg call');
	var that = this;
	
	$scope.data = [
		[],
	];

	that.meta = {
		type: 'ppg',
	};

	that.save = function() {

		if(that.meta.title
			&& that.meta.date) {

			let req = {
				method: 'POST',
				url: '/api/v1/workout',
				headers: {
					'Content-Type': 'application/json'
				},
				data: { 
					title: that.meta.title,
					type: that.meta.type,
					date: moment(that.meta.date).format('YYYY-MM-DD'),
					content: that.meta.content,
					steps: null
				 }
			};

			console.log(req);

			$http(req)
			 .then(function(result) {
				 console.log('success', result);
				 window.location = '/workout/detail/'+result.data.uuid;
			 }, function(error) {
				 console.log('error', error);
			 });

		} else {
			alert('Titre et date obligatoires');
		}

		return;
	}

}


function workoutDirective() {
	return {
		restrict: 'E',
		scope: {
			psteps: '=steps',
			wcr: '=',
		},
		templateUrl : '/ng-templates/workout-directive-template.html',
		controller: function() {
            console.log('inside controller for alerts Directive:');
            
        }

	}
}


// Polyfill
CanvasRenderingContext2D.prototype.clear = 
  CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
    if (preserveTransform) {
      this.save();
      this.setTransform(1, 0, 0, 1, 0, 0);
    }

    this.clearRect(0, 0, this.canvas.width, this.canvas.height);

    if (preserveTransform) {
      this.restore();
    }           
};

})();