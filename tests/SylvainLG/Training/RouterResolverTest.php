<?php
declare(strict_types=1);
namespace SylvainLG\Training;

use PHPUnit\Framework\TestCase;

use Pimple\Container;
use \SylvainLG\Training\PimpleProvider;

/**
 * @cover RouterResolver
 * @uses Pimple\Container
 * @uses SylvainLG\Training\PimpleProvider
 * @uses SylvainLG\Training\Controller\HomeController
 */
final class RouterResolverTest extends TestCase {

	private $container;

	public function setUp() {

		$this->container = new Container();
		$this->container->register(new PimpleProvider());

	}

	public function testCanCreateRouteResolver() {

		$this->assertInstanceOf(
			RouterResolver::class,
			new RouterResolver($this->container)
		);

	}

	/**
	 * @uses SylvainLG\Training\Controller\HomeController
	 */
	public function testCanResolveHandler() {

		$handler = ['SylvainLG\\Training\\Controller\\HomeController', 'index'];

		$routerResolver = new RouterResolver($this->container);

		$this->assertInstanceOf(
			\SylvainLG\Training\Controller\HomeController::class,
			$routerResolver->resolve($handler)[0]
		);

	}

	public function testCannotResolveWithWrongHandler() {

		$handler = 'whatever';

		$routerResolver = new RouterResolver($this->container);
		$result = $routerResolver->resolve($handler)[0];

		$this->assertNotInstanceOf(
			\SylvainLG\Training\Controller\HomeController::class,
			$result
		);

		$this->assertEquals(
			'w',
			$result
		);


		$handler = [1, 'machin'];

		$routerResolver = new RouterResolver($this->container);
		$result = $routerResolver->resolve($handler)[0];

		$this->assertNotInstanceOf(
			\SylvainLG\Training\Controller\HomeController::class,
			$result
		);

		$this->assertEquals(
			1,
			$result
		);

	}

	/*
	=> Fonction privée, pas de test 
	public function testEndsWith() {

		$haystack = 'abcdef';
		$needle = 'def';

		$routerResolver = new RouterResolver($this->container);

		$class = new ReflectionClass(RouterResolver::class);
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		$obj = new $classname($params);
		return $method->invokeArgs($obj, $params);


		$this->assertTrue($routerResolver->endsWith($haystack, $needle));

		$haystack = 'pouet';
		$this->assertTrue($routerResolver->endsWith($haystack, $needle));

	}
	*/

}
