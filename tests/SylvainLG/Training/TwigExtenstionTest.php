<?php
declare(strict_types=1);
namespace SylvainLG\Training;

use PHPUnit\Framework\TestCase;
use Pimple\Container;
use \SylvainLG\Training\PimpleProvider;

$_SESSION = [];

/**
 * @cover TwigExtension
 */
final class TwigExtensionTest extends TestCase {

	private $twigExtension;

	public function setUp() {
		$container = new Container();
		$container->register(new PimpleProvider());

		$this->twigExtension = new TwigExtension();
		$this->twigExtension->setContainer($container);
	}

	public function testIsFunctionsGetArray() {

		$this->assertEquals(
			gettype([]),
			gettype($this->twigExtension->getFunctions())
		);

	}

	public function testFunctionsReturnArrayOfTwigFunction() {

		$this->assertContainsOnlyInstancesOf(
			\Twig_SimpleFunction::class,
			$this->twigExtension->getFunctions()
		);

	}

	public function testGetGlobalsReturnArray() {

		$this->assertEquals(
			gettype([]),
			gettype($this->twigExtension->getGlobals())
		);

	}

	public function testGetGlobalsReturnedArrayContainsKeys() {

		$globs = $this->twigExtension->getGlobals();

		$this->assertArrayHasKey(
			'app',
			$globs
		);

		$this->assertArrayHasKey(
			'session',
			$globs['app']
		);
		
	}

	public function testIsFiltersGetArray() {

		$this->assertEquals(
			gettype([]),
			gettype($this->twigExtension->getFilters())
		);

	}

	public function testFiltersReturnArrayOfTwigFilter() {

		$this->assertContainsOnlyInstancesOf(
			\Twig_SimpleFilter::class,
			$this->twigExtension->getFilters()
		);

	}

	public function testGetName() {
		
		$this->assertEquals(
			'sylvainlg_extension',
			$this->twigExtension->getName()
		);

	}

	/*
	public function testRoute() {

		$this->assertEquals(
			'',
			$this->twigExtension->route('home')
		);

	}
	*/

}