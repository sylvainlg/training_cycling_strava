<?php
declare(strict_types=1);
namespace SylvainLG\Training;

use PHPUnit\Framework\TestCase;
use Pimple\Container;
use \SylvainLG\Training\PimpleProvider;

/**
 * @cover TwigHelper
 */
final class TwigHelperTest extends TestCase {

	private $twigHelper;

	public function setUp() {
		$container = new Container();
		$container->register(new PimpleProvider());

		$this->twigHelper = new TwigHelper();
		$this->twigHelper->setContainer($container);
	}

	public function testCanGenerateGmdate() {

		$format = 'dd/mm/YYYY';
		$timestamp = time();

		$this->assertEquals(
			gmdate($format, $timestamp),
			$this->twigHelper->gmdate($format, $timestamp)
		);
	}

	public function testIsFirstLetterUpperCase() {

		$this->assertEquals(
			'Bonjour les mecs',
			$this->twigHelper->ucfirst('bonjour les mecs')
		);

		$this->assertEquals(
			'Bonjour les mecs',
			$this->twigHelper->ucfirst('Bonjour les mecs')
		);
		
	}

}